#!/usr/bin/env bash

export AWS_DEFAULT_OUTPUT="table"
export AWS_PAGER=""
tg_arn=""

function install_aws_cli() {
    echo "== installing aws cli on ${OSTYPE}"
    if [[ "${OSTYPE}" == "linux-gnu"* ]]; then
            curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
            unzip awscliv2.zip && \
            sudo ./aws/install --update
    elif [[ "${OSTYPE}" == "darwin"* ]]; then
            curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg" && \
            sudo installer -pkg AWSCLIV2.pkg -target /
    else
            echo "OS unknown"
            exit 1
    fi
}

function install_jq() {
    echo "== installing jq on ${OSTYPE}"
    if [[ "${OSTYPE}" == "linux-gnu"* ]]; then
            sudo apt install jq -y
    elif [[ "${OSTYPE}" == "darwin"* ]]; then
            echo "not implemented"
    else
            echo "OS unknown"
            exit 1
    fi
}

function check_aws_cli() {
  echo "== checking if AWS CLI exist"
  if [[ "$(aws --version)" =~ aws-cli* ]]; then
    return 0
  else
    return 1
  fi
}

function check_jq() {
  echo "== checking if jq exist"
  which "jq"
  return $?
}

function cleanup() {
    echo "== cleaning up"
    rm -rf awscliv2.zip 2>/dev/null && \
    rm -rf aws 2>/dev/null && \
    rm -f temp.json 2>/dev/null
}

function get_credentials(){
  echo "ENTER AWS_ACCESS_KEY_ID: "
  read -r -s ACCESS_KEY_ID
  export AWS_ACCESS_KEY_ID=${ACCESS_KEY_ID}

  echo "ENTER AWS_SECRET_ACCESS_KEY: "
  read -r -s SECRET_ACCESS_KEY
  export AWS_SECRET_ACCESS_KEY=${SECRET_ACCESS_KEY}

  echo "ENTER AWS REGION: "
  read -r REGION
  AWS_DEFAULT_REGION=${REGION}
}

function check_cluster() {
    echo "== checking if cluster calligo exist"
    CLUSTER_LIST=$(aws ecs list-clusters --no-paginate)
    if [[ ${CLUSTER_LIST} =~ cluster/calligo ]];then
      return 0
    else
        echo ">>> cluster doesn't exist <<<"
        return 1
    fi
}

function delete_dns_record(){
  echo "== deleting dns record"
  aws route53 change-resource-record-sets --hosted-zone-id Z0332507E0IGPCHVWVKU \
  --change-batch '{ "Comment": "record for simpleapp service",
  "Changes": [ { "Action": "DELETE", "ResourceRecordSet": { "Name":
  "api-simpleapp.calligo.dev", "Type": "A",
        "AliasTarget":{
            "HostedZoneId": "Z35SXDOTRQ7X7K",
            "DNSName": "dualstack.smshub-1447571642.us-east-1.elb.amazonaws.com",
            "EvaluateTargetHealth": false
                    }
            } }
            ]
    }' --no-paginate
}

function check_credentials() {
  aws --region us-east-1 sts get-caller-identity  --endpoint-url https://sts.us-east-1.amazonaws.com --no-paginate
}

function delete_target_group() {
  echo "== deleting target group"
  tg_arn=$(aws elbv2 describe-target-groups --names simpleapp-api | grep 'TargetGroupArn' | cut -d '|' -f 4 | tr -d ' ')
  echo "${tg_arn}"
  if [[ ${tg_arn} == *"elasticloadbalancing"* ]];then
      aws elbv2 delete-target-group \
        --target-group-arn ${tg_arn}
  fi
}

function delete_elb_rule() {
  echo "== deleting elb rule"
  if [[ -f rule_arn.tmp ]];then
    rule_arn=$(cat rule_arn.tmp)
    rm rule_arn.tmp
    aws elbv2 delete-rule --rule-arn ${rule_arn}
  else
    return 1
  fi
}

function delete_service() {
  echo "== deleting service"
  service_resp=$(aws ecs describe-services --cluster calligo --services simpleapp-api)
  if ! [[ ${service_resp} == *"MISSING"* ]];then
      aws ecs delete-service \
        --cluster calligo  \
        --service simpleapp-api \
        --force
  fi
}

# check credentials
check_credentials || \
# get aws credentials
get_credentials && \
# check aws cli
check_aws_cli || \
# install aws cli
install_aws_cli && \
# check jq
check_jq|| \
# install aws cli
install_jq && \
# check cluster (ecs)
check_cluster && \
# delete dns record to resource(alb)
delete_dns_record
# delete elb rule
delete_elb_rule
# delete target group
delete_target_group
# delete service
delete_service

cleanup
