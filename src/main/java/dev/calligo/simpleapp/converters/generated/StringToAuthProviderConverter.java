package dev.calligo.simpleapp.converters.generated;

import org.springframework.core.convert.converter.Converter;
import dev.calligo.simpleapp.enums.generated.AuthProvider;

public class StringToAuthProviderConverter implements Converter<String, AuthProvider> {
  @Override
  public AuthProvider convert(String source) {
    try {
      return AuthProvider.valueOf(source.toUpperCase());
    } catch (IllegalArgumentException e) {
      return null;
    }
  }
}
