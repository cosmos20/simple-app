package dev.calligo.simpleapp.enums.generated;

import com.google.gson.annotations.SerializedName;

public enum MenuType {
  Url,
  Entity,
  Label;
}
