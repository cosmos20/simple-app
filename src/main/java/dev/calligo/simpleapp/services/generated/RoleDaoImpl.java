package dev.calligo.simpleapp.services.generated;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.GsonBuilder;
import dev.calligo.simpleapp.exceptions.global.NotFoundException;
import dev.calligo.simpleapp.utils.EntitiesUtil;
import dev.calligo.simpleapp.models.generated.EntityDef;

import dev.calligo.simpleapp.models.generated.FieldDef;
import dev.calligo.simpleapp.models.generated.Role;
import dev.calligo.simpleapp.search.SearchCriteria;
import dev.calligo.simpleapp.utils.Tools;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.atomic.AtomicReference;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;

import java.util.*;
import java.util.Map;

@Repository
@Qualifier("RoleDao")
public class RoleDaoImpl implements RoleDao {

  private static final Logger logger = LoggerFactory.getLogger("STDOUT-LOGGER");



  @Autowired MongoTemplate mongoTemplate;
  @Autowired private MongoOperations mongoOperations;
  @Autowired EntitiesUtil entitiesUtil;
  @Autowired Tools tools;
  
  Gson gson =
      new GsonBuilder()
          .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
          .serializeNulls()
          .create();
  final String COLLECTION = "Role";

  @Override
  @CachePut(value = "Role", key = "#Role.id", condition = "#Role.id != null")
  public Role create(Role Role) {
    if (Role.getId() == null) Role.setId(String.valueOf(UUID.randomUUID()));
    if (Role.getId().length() < 5 || !Role.getId().contains("-"))
      Role.setId(
          String.valueOf(
              UUID.randomUUID())); // else System.out.println("Role.getId() = " + Role.getId());

    return mongoTemplate.insert(Role, COLLECTION);
  }

  @Override
  public List<Role> create(List<Role> Role) {
    Role.stream()
        .forEach(
            s -> {
              if (s.getId() == null) {
                s.setId(String.valueOf(UUID.randomUUID()));
              }
            });
    return (List<Role>) mongoTemplate.insert(Role, COLLECTION);
  }

  @Override
  @CachePut(value = "Role", key = "#Role.id", condition = "#Role.id != null")
  public Role update(Role Role) {


    return mongoTemplate.save(Role, COLLECTION);
  }

  @Override
  @CacheEvict(value = "Role", key = "#id")
  public void delete(String id) {
    new Criteria();
    Query query = new Query().addCriteria(Criteria.where("_id").is(id));
    mongoTemplate.remove(query, COLLECTION);
    Role Role = new Role();
    Role.setId(id);
  }

  @Override
  public void deleteMany(List<String> ids) {
    Query query = new Query();
    new Criteria();
    query.addCriteria(Criteria.where("_id").in(ids));
    mongoTemplate.remove(query, COLLECTION);
  }

  @Override
  public void deleteAll() {

    mongoTemplate.remove(new Query(), COLLECTION);
  }

  @Override
  public Page<Role> search(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      int page,
      int size,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield) {
    try {
      Query query2 = new Query();
      if (hasfield && !isAdmin.get()) query2.addCriteria(Criteria.where("ownerId").is(userid));

      for (SearchCriteria tmp : params) {
        EntityDef en;
        if (entitiesUtil.getEntityMap().containsKey("Role")) {
          en = entitiesUtil.getEntityMap().get("Role");
        } else {
          throw new NotFoundException("entity not found");
        }
        FieldDef fieldDef = null;
        for (FieldDef fd : en.getFields()) {
          if (fd.getFriendlyName().equals(tmp.getKey().split("\\.")[0])) {
            fieldDef = fd;
          }
        }
        if (fieldDef == null) {
          throw new NotFoundException("field def not found");
        }
        String key = fieldDef.getFriendlyName();
        String value = (String) tmp.getValue();
        if (fieldDef.getType().contains(".")) {
          String entity = fieldDef.getType().split("\\.")[0];
          Query refQuery = new Query();
          refQuery.fields().include("_id");
          if (fieldDef.getType().contains(".id")) {
            refQuery.addCriteria(Criteria.where("_id").is(value));

          } else {
            refQuery.addCriteria(Criteria.where("friendlyName").is(value));
          }
          List<Map> refList =
              mongoTemplate.find(refQuery, Map.class, tools.toDashLimitedSingular(entity));
          if (refList.size() == 0) {
            throw new NotFoundException("ref object not found");
          }
          value = (String) refList.get(0).get("_id");
        } else {
          key = tmp.getKey();
        }
        boolean isPrimitive = entitiesUtil.isPrimitiveDataType(fieldDef.getType());
        boolean isArray = entitiesUtil.isArrayType(fieldDef.getType());
        switch (tmp.getOperation()) {
          case ":":
            if (isPrimitive) {
              switch (fieldDef.getType()) {
                case "int":
                case "Integer":
                case "integer":
                  int valueInt = Integer.parseInt(value);
                  query2.addCriteria(Criteria.where(key).is(valueInt));
                  break;
                case "double":
                case "Double":
                  double valueDouble = Double.parseDouble(value);
                  query2.addCriteria(Criteria.where(key).is(valueDouble));
                  break;
                case "boolean":
                case "Boolean":
                case "bool":
                  boolean valueBool = Boolean.parseBoolean(value);
                  query2.addCriteria(Criteria.where(key).is(valueBool));
                  break;
                default:
                  query2.addCriteria(Criteria.where(key).is(value));
                  break;
              }
            } else {
              if (isArray) {
                for (String item : (value).split(",")) {
                  query2.addCriteria(Criteria.where(key).is(item));
                }
              } else {
                query2.addCriteria(Criteria.where(key).is(value));
              }
            }
            break;
          case ">":
            if (isPrimitive) {
              switch (fieldDef.getType()) {
                case "int":
                case "Integer":
                case "integer":
                  int valueInt = Integer.parseInt(value);
                  query2.addCriteria(Criteria.where(key).gt(valueInt));
                  break;
                case "double":
                case "Double":
                  double valueDouble = Double.parseDouble(value);
                  query2.addCriteria(Criteria.where(key).gt(valueDouble));
                  break;
                case "boolean":
                case "Boolean":
                case "bool":
                  boolean valueBool = Boolean.parseBoolean(value);
                  query2.addCriteria(Criteria.where(key).gt(valueBool));

                  break;
                default:
                  query2.addCriteria(Criteria.where(key).gt(value));
                  break;
              }
            } else {
              if (isArray) {
                for (String item : (value).split(",")) {
                  query2.addCriteria(Criteria.where(key).gt(item));
                }
              } else {
                query2.addCriteria(Criteria.where(key).gt(value));
              }
            }
            break;
          case "<":
            if (isPrimitive) {
              switch (fieldDef.getType()) {
                case "int":
                case "Integer":
                case "integer":
                  int valueInt = Integer.parseInt(value);
                  query2.addCriteria(Criteria.where(key).lt(valueInt));
                  break;
                case "double":
                case "Double":
                  double valueDouble = Double.parseDouble(value);
                  query2.addCriteria(Criteria.where(key).lt(valueDouble));
                  break;
                case "boolean":
                case "Boolean":
                case "bool":
                  boolean valueBool = Boolean.parseBoolean(value);
                  query2.addCriteria(Criteria.where(key).lt(valueBool));

                  break;
                default:
                  query2.addCriteria(Criteria.where(key).lt(value));
                  break;
              }
            } else {
              if (isArray) {
                for (String item : (value).split(",")) {
                  query2.addCriteria(Criteria.where(key).lt(item));
                }
              } else {
                query2.addCriteria(Criteria.where(key).lt(value));
              }
            }
            break;
          case "~":
            query2.addCriteria(Criteria.where(key).regex(value));
            break;
        }
      }
      if (page > 0) {
        page--;
      }
      final Pageable pageableRequest =
          PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(direction), orderBy));
      query2.with(pageableRequest);
      List<Role> result = mongoTemplate.find(query2, Role.class, COLLECTION);
      result = fullFillReq(result);
      Long numOfDocumentCount =
          this.count(params, orderBy, direction, size, userid, isAdmin, hasfield);
      return PageableExecutionUtils.getPage(result, pageableRequest, () -> numOfDocumentCount);
    } catch (Exception ex) {
      logger.error(ex.getMessage());
      final Pageable pageableRequest = PageRequest.of(0, 10);
      return PageableExecutionUtils.getPage(Collections.emptyList(), pageableRequest, () -> 0L);
    }
  }

  @Override
  public long count(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      int size,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield) {
    try {
      Query query2 = new Query();
      if (hasfield && !isAdmin.get()) query2.addCriteria(Criteria.where("ownerId").is(userid));

      for (SearchCriteria tmp : params) {
        EntityDef en;
        if (entitiesUtil.getEntityMap().containsKey("Role")) {
          en = entitiesUtil.getEntityMap().get("Role");
        } else {
          throw new NotFoundException("entity not found");
        }
        FieldDef fieldDef = null;
        for (FieldDef fd : en.getFields()) {
          if (fd.getFriendlyName().equals(tmp.getKey().split("\\.")[0])) {
            fieldDef = fd;
          }
        }
        if (fieldDef == null) {
          throw new NotFoundException("field def not found");
        }
        String key = fieldDef.getFriendlyName();
        String value = (String) tmp.getValue();
        if (fieldDef.getType().contains(".")) {
          String entity = fieldDef.getType().split("\\.")[0];
          Query refQuery = new Query();
          refQuery.fields().include("_id");
          if (fieldDef.getType().contains(".id")) {
            refQuery.addCriteria(Criteria.where("_id").is(value));

          } else {
            refQuery.addCriteria(Criteria.where("friendlyName").is(value));
          }
          List<Map> refList =
              mongoTemplate.find(refQuery, Map.class, tools.toDashLimitedSingular(entity));
          if (refList.size() == 0) {
            throw new NotFoundException("ref object not found");
          }
          value = (String) refList.get(0).get("_id");
        } else {
          key = tmp.getKey();
        }
        boolean isPrimitive = entitiesUtil.isPrimitiveDataType(fieldDef.getType());
        boolean isArray = entitiesUtil.isArrayType(fieldDef.getType());
        switch (tmp.getOperation()) {
          case ":":
            if (isPrimitive) {
              switch (fieldDef.getType()) {
                case "int":
                case "Integer":
                case "integer":
                  int valueInt = Integer.parseInt(value);
                  query2.addCriteria(Criteria.where(key).is(valueInt));
                  break;
                case "double":
                case "Double":
                  double valueDouble = Double.parseDouble(value);
                  query2.addCriteria(Criteria.where(key).is(valueDouble));
                  break;
                case "boolean":
                case "Boolean":
                case "bool":
                  boolean valueBool = Boolean.parseBoolean(value);
                  query2.addCriteria(Criteria.where(key).is(valueBool));
                  break;
                default:
                  query2.addCriteria(Criteria.where(key).is(value));
                  break;
              }
            } else {
              if (isArray) {
                for (String item : (value).split(",")) {
                  query2.addCriteria(Criteria.where(key).is(item));
                }
              } else {
                query2.addCriteria(Criteria.where(key).is(value));
              }
            }
            break;
          case ">":
            if (isPrimitive) {
              switch (fieldDef.getType()) {
                case "int":
                case "Integer":
                case "integer":
                  int valueInt = Integer.parseInt(value);
                  query2.addCriteria(Criteria.where(key).gt(valueInt));
                  break;
                case "double":
                case "Double":
                  double valueDouble = Double.parseDouble(value);
                  query2.addCriteria(Criteria.where(key).gt(valueDouble));
                  break;
                case "boolean":
                case "Boolean":
                case "bool":
                  boolean valueBool = Boolean.parseBoolean(value);
                  query2.addCriteria(Criteria.where(key).gt(valueBool));

                  break;
                default:
                  query2.addCriteria(Criteria.where(key).gt(value));
                  break;
              }
            } else {
              if (isArray) {
                for (String item : (value).split(",")) {
                  query2.addCriteria(Criteria.where(key).gt(item));
                }
              } else {
                query2.addCriteria(Criteria.where(key).gt(value));
              }
            }
            break;
          case "<":
            if (isPrimitive) {
              switch (fieldDef.getType()) {
                case "int":
                case "Integer":
                case "integer":
                  int valueInt = Integer.parseInt(value);
                  query2.addCriteria(Criteria.where(key).lt(valueInt));
                  break;
                case "double":
                case "Double":
                  double valueDouble = Double.parseDouble(value);
                  query2.addCriteria(Criteria.where(key).lt(valueDouble));
                  break;
                case "boolean":
                case "Boolean":
                case "bool":
                  boolean valueBool = Boolean.parseBoolean(value);
                  query2.addCriteria(Criteria.where(key).lt(valueBool));

                  break;
                default:
                  query2.addCriteria(Criteria.where(key).lt(value));
                  break;
              }
            } else {
              if (isArray) {
                for (String item : (value).split(",")) {
                  query2.addCriteria(Criteria.where(key).lt(item));
                }
              } else {
                query2.addCriteria(Criteria.where(key).lt(value));
              }
            }
            break;
          case "~":
            query2.addCriteria(Criteria.where(key).regex(value));
            break;
        }
      }

      //     final Pageable pageableRequest =
      //         PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(direction), orderBy));
      //      query2.with(pageableRequest);
      long result = mongoTemplate.count(query2, Role.class, COLLECTION);

      //             result = fullFillReq(result);

      return result;
    } catch (Exception ex) {
      logger.error(ex.getMessage());
      //     final Pageable pageableRequest = PageRequest.of(0, 10);
      //    return PageableExecutionUtils.getPage(Collections.emptyList(), pageableRequest, () ->
      // 0L);
      return -1;
    }
  }

  @Override
  public List<Role> search(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield) {
    try {
      Query query = new Query();
      if (hasfield && !isAdmin.get()) query.addCriteria(Criteria.where("ownerId").is(userid));

      for (SearchCriteria tmp : params) {
        EntityDef en;
        if (entitiesUtil.getEntityMap().containsKey("Role")) {
          en = entitiesUtil.getEntityMap().get("Role");
        } else {
          throw new NotFoundException("entity not found");
        }
        FieldDef fieldDef = null;
        for (FieldDef fd : en.getFields()) {
          if (fd.getFriendlyName().equals(tmp.getKey().split("\\.")[0])) {
            fieldDef = fd;
          }
        }
        if (fieldDef == null) {
          throw new NotFoundException("field def not found");
        }
        String key = fieldDef.getFriendlyName();
        String value = (String) tmp.getValue();
        if (fieldDef.getType().contains(".")) {
          String entity = fieldDef.getType().split("\\.")[0];
          Query refQuery = new Query();
          refQuery.fields().include("_id");
          if (fieldDef.getType().contains(".id")) {
            refQuery.addCriteria(Criteria.where("_id").is(value));

          } else {
            refQuery.addCriteria(Criteria.where("friendlyName").is(value));
          }
          List<Map> refList =
              mongoTemplate.find(refQuery, Map.class, tools.toDashLimitedSingular(entity));
          if (refList.size() == 0) {
            throw new NotFoundException("ref object not found");
          }
          value = (String) refList.get(0).get("_id");
        } else {
          key = tmp.getKey();
        }
        boolean isPrimitive = entitiesUtil.isPrimitiveDataType(fieldDef.getType());
        boolean isArray = entitiesUtil.isArrayType(fieldDef.getType());
        switch (tmp.getOperation()) {
          case ":":
            if (isPrimitive) {
              switch (fieldDef.getType()) {
                case "int":
                case "Integer":
                case "integer":
                  int valueInt = Integer.parseInt(value);
                  query.addCriteria(Criteria.where(key).is(valueInt));
                  break;
                case "double":
                case "Double":
                  double valueDouble = Double.parseDouble(value);
                  query.addCriteria(Criteria.where(key).is(valueDouble));
                  break;
                case "boolean":
                case "Boolean":
                case "bool":
                  boolean valueBool = Boolean.parseBoolean(value);
                  query.addCriteria(Criteria.where(key).is(valueBool));
                  break;
                default:
                  query.addCriteria(Criteria.where(key).is(value));
                  break;
              }
            } else {
              if (isArray) {
                for (String item : (value).split(",")) {
                  query.addCriteria(Criteria.where(key).is(item));
                }
              } else {
                query.addCriteria(Criteria.where(key).is(value));
              }
            }
            break;
          case ">":
            if (isPrimitive) {
              switch (fieldDef.getType()) {
                case "int":
                case "Integer":
                case "integer":
                  int valueInt = Integer.parseInt(value);
                  query.addCriteria(Criteria.where(key).gt(valueInt));
                  break;
                case "double":
                case "Double":
                  double valueDouble = Double.parseDouble(value);
                  query.addCriteria(Criteria.where(key).gt(valueDouble));
                  break;
                case "boolean":
                case "Boolean":
                case "bool":
                  boolean valueBool = Boolean.parseBoolean(value);
                  query.addCriteria(Criteria.where(key).gt(valueBool));

                  break;
                default:
                  query.addCriteria(Criteria.where(key).gt(value));
                  break;
              }
            } else {
              if (isArray) {
                for (String item : (value).split(",")) {
                  query.addCriteria(Criteria.where(key).gt(item));
                }
              } else {
                query.addCriteria(Criteria.where(key).gt(value));
              }
            }
            break;
          case "<":
            if (isPrimitive) {
              switch (fieldDef.getType()) {
                case "int":
                case "Integer":
                case "integer":
                  int valueInt = Integer.parseInt(value);
                  query.addCriteria(Criteria.where(key).lt(valueInt));
                  break;
                case "double":
                case "Double":
                  double valueDouble = Double.parseDouble(value);
                  query.addCriteria(Criteria.where(key).lt(valueDouble));
                  break;
                case "boolean":
                case "Boolean":
                case "bool":
                  boolean valueBool = Boolean.parseBoolean(value);
                  query.addCriteria(Criteria.where(key).lt(valueBool));

                  break;
                default:
                  query.addCriteria(Criteria.where(key).lt(value));
                  break;
              }
            } else {
              if (isArray) {
                for (String item : (value).split(",")) {
                  query.addCriteria(Criteria.where(key).lt(item));
                }
              } else {
                query.addCriteria(Criteria.where(key).lt(value));
              }
            }
            break;
          case "~":
            query.addCriteria(Criteria.where(key).regex(value));
            break;
        }
      }
      return mongoTemplate.find(query, Role.class, COLLECTION);
    } catch (Exception ex) {
      logger.error(ex.getMessage());
      return Collections.emptyList();
    }
  }

  @Override
  @Cacheable(
      value = "Role",
      key = "#id",
      condition = "#id != null and #cache == true",
      unless = "#result == null")
  public Role find(
      String id, boolean cache, String userid, AtomicReference<Boolean> isAdmin, Boolean hasfield) {
    Query query =
        new Query(Criteria.where("_id").is(id).andOperator(Criteria.where("ownerId").is(userid)));
    if (isAdmin.get()) query = new Query(Criteria.where("_id").is(id));
    if (!hasfield) query = new Query(Criteria.where("_id").is(id));

    return fullFillReq(mongoTemplate.findOne(query, Role.class, COLLECTION));
  }

  @Override
  public Role findBy(String fieldName, String value) {
    if (fieldName.equals("id")) {
      fieldName = "_id";
    }
    Query query = new Query(Criteria.where(fieldName).is(value));
    return mongoTemplate.findOne(query, Role.class, COLLECTION);
  }

  @Override
  public List<Role> getall(ArrayList<String> fieldNames) {
    List<Role> et = new ArrayList<>();
    try {
      Query query = new Query();
      if (fieldNames != null && fieldNames.size() > 0)
        fieldNames.stream()
            .forEach(
                s -> {
                  query.fields().include(s);
                });
      et = mongoTemplate.find(query, Role.class, COLLECTION);
    } catch (Exception e) {
      //   e.printStackTrace();
    }
    return et;
  }

  private Role fullFillReq(Role one) {
    return one;
  }

  private List<Role> fullFillReq(List<Role> ones) {
    List<Role> ones2 = new ArrayList<>();
    for (Role one : ones) {
      Role one2 = fullFillReq(one);
      ones2.add(one2);
    }
    return ones2;
  }
}
