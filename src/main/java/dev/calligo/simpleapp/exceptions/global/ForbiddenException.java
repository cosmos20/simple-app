package dev.calligo.simpleapp.exceptions.global;

public class ForbiddenException extends RuntimeException {
  public ForbiddenException(String msg) {
    super(msg);
  }
}
