package dev.calligo.simpleapp.enums.generated;

import com.google.gson.annotations.SerializedName;

public enum JustifyEnum {
  Start,
  End,
  Between,
  Center;
}
