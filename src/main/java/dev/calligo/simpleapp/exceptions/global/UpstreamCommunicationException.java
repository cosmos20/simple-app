package dev.calligo.simpleapp.exceptions.global;

public class UpstreamCommunicationException extends RuntimeException {
  public UpstreamCommunicationException(String msg) {
    super(msg);
  }
}
