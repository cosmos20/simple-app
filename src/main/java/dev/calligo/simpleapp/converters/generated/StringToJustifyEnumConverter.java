package dev.calligo.simpleapp.converters.generated;

import org.springframework.core.convert.converter.Converter;
import dev.calligo.simpleapp.enums.generated.JustifyEnum;

public class StringToJustifyEnumConverter implements Converter<String, JustifyEnum> {
  @Override
  public JustifyEnum convert(String source) {
    try {
      return JustifyEnum.valueOf(source.toUpperCase());
    } catch (IllegalArgumentException e) {
      return null;
    }
  }
}
