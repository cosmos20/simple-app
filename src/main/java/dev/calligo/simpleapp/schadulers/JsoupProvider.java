package dev.calligo.simpleapp.schadulers;


import dev.calligo.simpleapp.models.generated.Merchant;
import dev.calligo.simpleapp.services.generated.MerchantDao;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class JsoupProvider  {
    private static final Logger log = LoggerFactory.getLogger("STDOUT-LOGGER");
    @Autowired
     MerchantDao merchantDao;


    public  void schedule() {
        log.info("Schaduler started ...");
        List<Merchant> merchants = getMerchatIds();
        if(merchants.size()>0){
            merchantDao.deleteAll();
            merchantDao.create(merchants);
            log.info(merchants.size()+" Merchants inserted.");
        }

    }


    public  List<Merchant> getMerchatIds(){
        List<Merchant> merchants = new ArrayList<>();

        try{
            Document doc = Jsoup.connect("https://guusto.com/merchants")
                    .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36")
                    .referrer("http://www.google.com")
                    .header("Accept-Language", "en-US,en;q=0.5")
                    .timeout(12000)
                    .followRedirects(true).get();
            Elements USA_merchants = doc.select("#USA-merchants").select("div[class=merchant-listing show col-lg-4 col-md-6 py-4]");
            Elements CAN_merchants = doc.select("#CAN-merchants").select("div[class=merchant-listing show col-lg-4 col-md-6 py-4]");
            Elements INTL_merchants = doc.select("#INTL-merchants").select("div[class=merchant-listing show col-lg-4 col-md-6 py-4]");
            printDetails(USA_merchants,merchants,"USA");
            printDetails(CAN_merchants,merchants,"CAN");
            printDetails(INTL_merchants,merchants,"INT");
        }catch (Exception e){
            e.printStackTrace();
        }
        log.info(merchants.size()+" Merchants read");
        return merchants;
    }

    private  void printDetails(Elements usa_merchants, List<Merchant> merchants, String country) {
        for (Element headline : usa_merchants) {
            String name = headline.select("div[class=merchant-name font-weight-bold]").text();
            String location = headline.select("div[class=merchant-location instore-online]").html();
            String domination = headline.select("p[class=merchant-denomination]").text();
            String image = headline.select("img[class=d-block]").attr("src");
            Merchant m = new Merchant(name
                    , location
                    , image
                    , domination
                    ,country);
            merchants.add(m);

        }
    }

    private  void log(String s, String title, String href) {
        log.info(s+" "+title+" "+href);

    }
}

