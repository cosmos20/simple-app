package dev.calligo.simpleapp.models.generated;

import com.github.javafaker.Faker;
import io.swagger.annotations.ApiModel;
import dev.calligo.simpleapp.enums.generated.*;
import lombok.Setter;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.*;
import org.springframework.http.MediaType;
import javax.validation.constraints.*;
import org.springframework.data.mongodb.core.mapping.Field;
import com.google.gson.annotations.SerializedName;

@ApiModel(description = "Menu model")
public class Menu implements Serializable {
  Map<String, ArrayList<String>> arrayofarray;

  @Getter
  @Setter
  @Field("_id")
  @Id
  @SerializedName("id")
  private String id;

  @Getter
  @Setter
  @SerializedName("OwnerId")
  private String OwnerId;

  @Getter
  @Setter
  @SerializedName("LastUpdateBy")
  private String LastUpdateBy;

  @Getter
  @Setter
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @SerializedName("DateUpdated")
  private Date DateUpdated;

  @Getter
  @Setter
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @SerializedName("DateAdded")
  private Date DateAdded;

  @NotNull(message = "This field is required")
  @Getter
  @Setter
  @SerializedName("FriendlyName")
  private String FriendlyName;

  @Getter
  @Setter
  @SerializedName("Order")
  private int Order;

  @Getter
  @Setter
  @SerializedName("Enabled")
  private Boolean Enabled;

  @NotNull(message = "This field is required")
  @Getter
  @Setter
  @SerializedName("Caption")
  private String Caption;

  @Getter
  @Setter
  @SerializedName("Url")
  private String Url;

  @Getter
  @Setter
  @SerializedName("Entity")
  private EntityDef Entity;

  @Getter
  @Setter
  @SerializedName("MenuType")
  private MenuType MenuType;

  @Getter @Setter private String ParentId;

  @NotNull(message = "This field is required")
  @Getter
  @Setter
  @SerializedName("Icon")
  private String Icon;

  @NotNull(message = "This field is required")
  @Getter
  @Setter
  @SerializedName("EndPoint")
  private String EndPoint;

  public Menu generateFake() {
    Faker faker = new Faker();
    Menu ret = new Menu();
    ret.setId(String.valueOf(UUID.randomUUID()));

    ret.setOwnerId("admin");

    ret.setLastUpdateBy(faker.name().name());

    ret.setDateUpdated(new Date());

    ret.setDateAdded(new Date());

    ret.setFriendlyName(faker.name().name());

    ret.setOrder(faker.number().randomDigit());

    ret.setEnabled(true);

    ret.setCaption(faker.name().name());

    ret.setUrl(faker.name().name());

    ret.setIcon(faker.name().name());

    ret.setEndPoint(faker.name().name());
    return ret;
  }

  public String toCSV(String attach) {
    String ret = attach;
    ret =
        ret
            + (ret.length() > 3 ? "," : "")
            + this.id
            + ","
            + this.OwnerId
            + ","
            + this.LastUpdateBy
            + ","
            + this.DateUpdated
            + ","
            + this.DateAdded
            + ","
            + this.FriendlyName
            + ","
            + this.Order
            + ","
            + this.Enabled
            + ","
            + this.Caption
            + ","
            + this.Url
            + ","
            + this.Entity.toCSV("")
            + ","
            + this.MenuType
            + ","
            + this.ParentId
            + ","
            + this.Icon
            + ","
            + this.EndPoint;
    return ret;
  }

  private String leverage(String line, Map<String, ArrayList<String>> arrayofarray) {
    String ret = "";
    Map<String, ArrayList<String>> arrayofarray2 = arrayofarray;
    ArrayList<String> arrayofarray3 = new ArrayList<>();
    arrayofarray3.addAll(arrayofarray.keySet());
    for (String s : arrayofarray3) {
      if (arrayofarray != null && arrayofarray.size() > 0) {
        for (String d : arrayofarray.get(s.toUpperCase())) {
          String ll = line.replaceAll(s.toUpperCase(), d);
          Map<String, ArrayList<String>> arrayofarray22 = arrayofarray2;
          arrayofarray22.remove(s);
          if (arrayofarray22.size() > 0) ret = ret + "" + leverage(ll, arrayofarray22);
          else ret = ret + "" + ll;
        }
      }
    }

    return ret;
  }

  private String toleverage(ArrayList<String> clientproducts, String replacement) {
    if (arrayofarray == null) arrayofarray = new HashMap<>();
    if (clientproducts != null && clientproducts.size() > 0) {
      ArrayList<String> datas = arrayofarray.get(replacement);
      if (datas == null) datas = new ArrayList<>();
      for (String s : clientproducts) datas.add(s);
      arrayofarray.put(replacement, datas);
    } else replacement = "";
    return replacement;
  }

  @Override
  public String toString() {
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      return objectMapper.writeValueAsString(this);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
      return null;
    }
  }
}
