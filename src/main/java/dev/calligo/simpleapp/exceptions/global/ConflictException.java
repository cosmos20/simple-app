package dev.calligo.simpleapp.exceptions.global;

public class ConflictException extends RuntimeException {
  public ConflictException(String msg) {
    super(msg);
  }
}
