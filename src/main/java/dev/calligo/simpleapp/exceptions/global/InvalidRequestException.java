package dev.calligo.simpleapp.exceptions.global;

public class InvalidRequestException extends RuntimeException {
  public InvalidRequestException(String msg) {
    super(msg);
  }
}
