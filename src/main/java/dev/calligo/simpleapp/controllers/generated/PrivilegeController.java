package dev.calligo.simpleapp.controllers.generated;

import dev.calligo.simpleapp.exceptions.global.ConflictException;
import dev.calligo.simpleapp.exceptions.global.InternalException;
import dev.calligo.simpleapp.exceptions.global.InvalidRequestException;
import dev.calligo.simpleapp.exceptions.global.NotFoundException;
import dev.calligo.simpleapp.utils.EntitiesUtil;
import dev.calligo.simpleapp.enums.generated.*;
import dev.calligo.simpleapp.converters.generated.*;
import dev.calligo.simpleapp.models.generated.Privilege;
import dev.calligo.simpleapp.search.SearchCriteria;
import dev.calligo.simpleapp.security.AuthenticationFacade;
import dev.calligo.simpleapp.services.generated.*;
import dev.calligo.simpleapp.utils.Exporter;
import dev.calligo.simpleapp.utils.Tools;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import dev.calligo.simpleapp.models.generated.*;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.beans.factory.annotation.Value;
import java.io.*;
import org.springframework.data.domain.Page;
import java.util.concurrent.atomic.AtomicReference;
import java.math.BigDecimal;

import javax.validation.Valid;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

// GENERATED IMPORTS

@RestController
@RequestMapping(value = "/Privilege", produces = "application/json")
public class PrivilegeController {

  @Autowired PrivilegeDao api;
  @Autowired AuthenticationFacade authenticationFacade;
  @Autowired PasswordEncoder encoder;
  @Autowired EntitiesUtil entitiesUtil;
  @Autowired Exporter exporter;
  @Autowired Tools tools;
  @Autowired PrivilegeConverter converter;

  @RequestMapping(method = RequestMethod.GET)
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  @PreAuthorize("hasAuthority('PRIVILEGE_READ_PRIVILEGE')")
  public ResponseEntity<?> getPrivileges(
      @RequestParam(value = "orderBy", required = false, defaultValue = "id") String orderBy,
      @RequestParam(value = "direction", required = false, defaultValue = "ASC") String direction,
      @RequestParam(value = "page", required = false, defaultValue = "0") int page,
      @RequestParam(value = "size", required = false, defaultValue = "20") int size,
      @RequestParam(value = "paginate", required = false, defaultValue = "true") boolean paginate,
      @RequestParam(value = "search", required = false) String search)
      throws Exception {
    Authentication authentication = authenticationFacade.getAuthentication();
    String userid = authentication.getName();
    AtomicReference<Boolean> isAdmin = new AtomicReference<>(false);
    authentication
        .getAuthorities()
        .forEach(
            s -> {
              // if (s.getAuthority().toLowerCase().contains("write") &&
              // s.getAuthority().toLowerCase().contains("Privilege")) isAdmin.set(true);
              if (userid.equals("admin")) isAdmin.set(true);
            });

    List<SearchCriteria> params = tools.getSearchCriteria(search);
    if (!paginate) {
      return ResponseEntity.ok()
          .body(
              api.search(params, orderBy, direction, 1, 1000, userid, isAdmin, false).getContent());
    }
    Page<Privilege> ret =
        api.search(params, orderBy, direction, page, size, userid, isAdmin, false);
    return ResponseEntity.ok().body(ret);
  }

  @GetMapping(path = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @PreAuthorize("hasAuthority('PRIVILEGE_READ_PRIVILEGE')")
  public ResponseEntity<Privilege> getPrivilege(@PathVariable String id) {
    Authentication authentication = authenticationFacade.getAuthentication();
    String userid = authentication.getName();
    AtomicReference<Boolean> isAdmin = new AtomicReference<>(false);
    authentication
        .getAuthorities()
        .forEach(
            s -> {
              //   if (s.getAuthority().toLowerCase().contains("write") &&
              // s.getAuthority().toLowerCase().contains("Privilege")) isAdmin.set(true);
              if (userid.equals("admin")) isAdmin.set(true);
            });
    Privilege item = api.find(id, true, userid, isAdmin, false);
    if (item == null) {
      throw new NotFoundException("id not found");
    }
    return ResponseEntity.ok().body(item);
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PreAuthorize("hasAuthority('PRIVILEGE_WRITE_PRIVILEGE')")
  public ResponseEntity<?> deletePrivilege(@PathVariable String id) {
    Authentication authentication = authenticationFacade.getAuthentication();
    String userid = authentication.getName();
    AtomicReference<Boolean> isAdmin = new AtomicReference<>(false);
    authentication
        .getAuthorities()
        .forEach(
            s -> {
              //     if (s.getAuthority().toLowerCase().contains("write") &&
              // s.getAuthority().toLowerCase().contains("Privilege")) isAdmin.set(true);
              if (userid.equals("admin")) isAdmin.set(true);
            });

    if (api.find(id, false, userid, isAdmin, false) == null) {

      throw new NotFoundException("id not found");
    }

    api.delete(id);

    //  return ResponseEntity.ok(new ApiResponse(true, "Privilege deleted successfully@"));
    return ResponseEntity.ok().body("Deleted");
  }

  @DeleteMapping("/ids")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PreAuthorize("hasAuthority('PRIVILEGE_WRITE_PRIVILEGE')")
  public ResponseEntity<?> deletePrivileges(@RequestParam List<String> ids) {
    api.deleteMany(ids);
    return ResponseEntity.noContent().build();
  }

  @RequestMapping(
      value = "export",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_PDF_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE})
  @ResponseStatus(HttpStatus.OK)
  @PreAuthorize("hasAuthority('PRIVILEGE_READ_PRIVILEGE')")
  public ResponseEntity<InputStreamResource> exportPrivileges(
      @RequestParam(value = "search", required = false) String search,
      @RequestParam(value = "orderBy", required = false, defaultValue = "id") String orderBy,
      @RequestParam(value = "direction", required = false, defaultValue = "ASC") String direction,
      @RequestParam(value = "fields", required = false) String fields,
      @RequestParam(value = "type") String type)
      throws Exception {
    Authentication authentication = authenticationFacade.getAuthentication();
    String userid = authentication.getName();
    AtomicReference<Boolean> isAdmin = new AtomicReference<>(false);
    authentication
        .getAuthorities()
        .forEach(
            s -> {
              // if (s.getAuthority().toLowerCase().contains("write") &&
              // s.getAuthority().toLowerCase().contains("Privilege")) isAdmin.set(true);
              if (userid.equals("admin")) isAdmin.set(true);
            });

    List<SearchCriteria> params = tools.getSearchCriteria(search);
    List<Privilege> Privileges =
        api.search(params, orderBy, direction, 1, 1000, userid, isAdmin, false).getContent();
    ByteArrayInputStream bis;
    if (Privileges.size() == 0) {
      bis = new ByteArrayInputStream(new byte[0]);
    } else {
      bis = exporter.export(new ArrayList<>(Privileges), fields, type);
    }

    HttpHeaders headers = new HttpHeaders();
    switch (type.toLowerCase()) {
      case "pdf":
        headers.add("Content-Disposition", "inline; filename=PrivilegesExport.pdf");
        return ResponseEntity.ok()
            .headers(headers)
            .contentType(MediaType.APPLICATION_PDF)
            .body(new InputStreamResource(bis));
      case "excel":
        headers.add("Content-Disposition", "inline; filename=PrivilegesExport.xls");
        return ResponseEntity.ok()
            .headers(headers)
            .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
            .body(new InputStreamResource(bis));
      default:
        throw new InvalidRequestException("invalid type");
    }
  }

  @GetMapping(value = "/getTemplate")
  @PreAuthorize("hasAuthority('PRIVILEGE_WRITE_PRIVILEGE')")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<?> getPrivilegeTemplate() throws Exception {
    HttpHeaders headers = new HttpHeaders();
    headers.add("Content-Disposition", "inline; filename=PrivilegeTemplate.csv");
    return ResponseEntity.ok()
        .headers(headers)
        .contentType(MediaType.APPLICATION_OCTET_STREAM)
        .body(new InputStreamResource(converter.getTemplate()));
  }

  @ResponseStatus(HttpStatus.OK)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public Map<String, String> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
    Map<String, String> errors = new HashMap<>();

    ex.getBindingResult()
        .getFieldErrors()
        .forEach(
            error -> {
              errors.put(error.getField(), error.getDefaultMessage());
              System.out.println(
                  "error.getField()::"
                      + error.getField()
                      + " error.getDefaultMessage()::"
                      + error.getDefaultMessage());
            });

    return errors;
  }

  // GENERATED METHODS
  @PutMapping("/{id}")
  @PreAuthorize("hasAuthority('PRIVILEGE_WRITE_PRIVILEGE')")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Privilege> updatePrivilege(
      @RequestBody Privilege Privilege, @PathVariable String id) throws Exception {
    Authentication authentication = authenticationFacade.getAuthentication();
    String userid = authentication.getName();
    AtomicReference<Boolean> isAdmin = new AtomicReference<>(false);
    authentication
        .getAuthorities()
        .forEach(
            s -> {
              //     if(s.getAuthority().toLowerCase().contains("admin")) isAdmin.set(true);
              if (userid.equals("admin")) isAdmin.set(true);
            });

    if (api.find(id, false, userid, isAdmin, false) == null) {
      throw new NotFoundException("id not found");
    }
    Privilege.setId(id);
    api.update(Privilege);

    return ResponseEntity.ok().body(Privilege);
  }

  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  @PreAuthorize("hasAuthority('PRIVILEGE_WRITE_PRIVILEGE')")
  public ResponseEntity<Privilege> createPrivilege(@Valid @RequestBody Privilege Privilege)
      throws Exception {
    if (api.findBy("id", Privilege.getId()) != null) {
      throw new ConflictException(
          "unique constraint violated, duplicate field: id -> " + Privilege.getId());
    }
    Authentication authentication = authenticationFacade.getAuthentication();
    String userid = authentication.getName();
    api.create(Privilege);

    return ResponseEntity.created(URI.create(Privilege.getId())).body(Privilege);
  }

  @PostMapping(value = "/import")
  @PreAuthorize("hasAuthority('PRIVILEGE_WRITE_PRIVILEGE')")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<List<Privilege>> importPrivileges(
      @RequestPart("file") MultipartFile filePart) throws Exception {

    List<Privilege> Privileges =
        converter.csvToPojo(Collections.singletonList(new String(filePart.getBytes())));
    Authentication authentication = authenticationFacade.getAuthentication();
    String userid = authentication.getName();

    for (Privilege Privilege : Privileges) {
      api.create(Privilege);
    }
    return ResponseEntity.ok(Privileges);
  }

  @Value("classpath:json/PRIVILEGE.json")
  Resource menufile;

  @GetMapping(path = "getEntityFields")
  @ResponseStatus(HttpStatus.OK)
  @PreAuthorize("hasAuthority('PRIVILEGE_READ_PRIVILEGE')")
  public ResponseEntity<String> getEntityFields() throws Exception {
    InputStream EntityListContent = menufile.getInputStream();
    StringBuilder fileContent = new StringBuilder();
    BufferedReader bufferedReader = null;
    try {
      bufferedReader =
          //        new BufferedReader(new FileReader(new
          // File(String.valueOf(menufile.getFile()))));
          new BufferedReader(new InputStreamReader(EntityListContent));

      String line;
      while ((line = bufferedReader.readLine()) != null) {
        fileContent.append(line);
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (bufferedReader != null) {
        try {
          bufferedReader.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    String arrayToJson = fileContent.toString();
    return ResponseEntity.ok().body(arrayToJson);
  }

  @PatchMapping(path = "/{id}")
  public ResponseEntity<Privilege> updatePrivilege(
      @PathVariable String id, @RequestBody Map<String, Object> patch) {
    try {
      Authentication authentication = authenticationFacade.getAuthentication();
      String userid = authentication.getName();
      AtomicReference<Boolean> isAdmin = new AtomicReference<>(false);
      authentication
          .getAuthorities()
          .forEach(
              s -> {
                //  if (s.getAuthority().toLowerCase().contains("admin")) isAdmin.set(true);
                if (userid.equals("admin")) isAdmin.set(true);
              });

      Privilege customer = api.find(id, false, userid, isAdmin, false);
      if (customer != null) {
        patch.forEach(
            (change, value) -> {
              switch (change) {
                case "id":
                  customer.setId((String) value);
                  break;
                case "Name":
                  customer.setName((String) value);
                  break;
                case "FriendlyName":
                  customer.setFriendlyName((String) value);
                  break;
              }
            });
        api.update(customer);
      }
      return ResponseEntity.ok(customer);
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }
}
