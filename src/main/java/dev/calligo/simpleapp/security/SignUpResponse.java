package dev.calligo.simpleapp.security;

import java.io.Serializable;
import java.util.Objects;

public class SignUpResponse implements Serializable {

    private static final long serialVersionUID = -8091879091924046844L;
    private final String jwttoken;
    private final String username;



    public  SignUpResponse(String jwttoken,String username) {
        this.jwttoken = jwttoken;
        this.username = username;
    }

    public String getToken() {
        return this.jwttoken;
    }
    public String getUserName() {
        return username;
    }
}
