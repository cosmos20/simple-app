package dev.calligo.simpleapp.services.generated;

import dev.calligo.simpleapp.models.generated.*;
import dev.calligo.simpleapp.models.generated.Role;
import dev.calligo.simpleapp.enums.generated.*;
import dev.calligo.simpleapp.search.SearchCriteria;
import org.springframework.data.domain.Page;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public interface RoleDao {

  /*
   create single Role
  */
  Role create(Role Role);

  /*
   create multiple Roles
  */
  List<Role> create(List<Role> Roles);

  /*
   update single Role
  */
  Role update(Role Role);

  /*
   delete single Role
  */
  void delete(String id);

  /*
   delete multiple Roles
  */
  void deleteMany(List<String> ids);

  /*
   delete all Roles
  */
  void deleteAll();

  /*
   find user by id
  */
  Role find(
      String Id, boolean cache, String userid, AtomicReference<Boolean> isAdmin, Boolean hasfield);

  /*
   search for Roles using search criteria
  */
  Page<Role> search(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      int page,
      int size,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  long count(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      int size,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  /*
   search for Roles using search criteria - non paginated
  */
  List<Role> search(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  /*
   find by field
  */
  Role findBy(String fieldName, String value);

  List<Role> getall(ArrayList<String> fieldNames);
}
