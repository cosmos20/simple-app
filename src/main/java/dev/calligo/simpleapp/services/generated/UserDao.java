package dev.calligo.simpleapp.services.generated;

import dev.calligo.simpleapp.models.generated.*;
import dev.calligo.simpleapp.models.generated.User;
import dev.calligo.simpleapp.enums.generated.*;
import dev.calligo.simpleapp.search.SearchCriteria;
import org.springframework.data.domain.Page;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public interface UserDao {

  /*
   create single User
  */
  User create(User User);

  /*
   create multiple Users
  */
  List<User> create(List<User> Users);

  /*
   update single User
  */
  User update(User User);

  /*
   delete single User
  */
  void delete(String id);

  /*
   delete multiple Users
  */
  void deleteMany(List<String> ids);

  /*
   delete all Users
  */
  void deleteAll();

  /*
   find user by id
  */
  User find(
      String Id, boolean cache, String userid, AtomicReference<Boolean> isAdmin, Boolean hasfield);

  /*
   search for Users using search criteria
  */
  Page<User> search(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      int page,
      int size,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  long count(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      int size,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  /*
   search for Users using search criteria - non paginated
  */
  List<User> search(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  /*
   find by field
  */
  User findBy(String fieldName, String value);

  List<User> getall(ArrayList<String> fieldNames);
}
