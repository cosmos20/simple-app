package dev.calligo.simpleapp;

import dev.calligo.simpleapp.conf.AppProperties;
import dev.calligo.simpleapp.schadulers.JsoupProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.io.Resource;
import com.google.gson.Gson;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.password.PasswordEncoder;
import dev.calligo.simpleapp.models.Entity;
import dev.calligo.simpleapp.models.EntityField;
import dev.calligo.simpleapp.models.generated.*;
import dev.calligo.simpleapp.services.generated.*;


import dev.calligo.simpleapp.utils.EntitiesUtil;
import dev.calligo.simpleapp.utils.Tools;
import dev.calligo.simpleapp.enums.generated.MenuType;


import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class Application implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger("STDOUT-LOGGER");



    @Value("classpath:/EntityNames.json")
    Resource entityFile;


    @Autowired JsoupProvider JsoupProvider;
    @Autowired PrivilegeDao privilegeDao;
    @Autowired EntityDefDao entityDefDao;
    @Autowired RoleDao roleDao;
    @Autowired UserDao userDao;
    @Autowired MenuDao menuDao;
    @Autowired EntitiesUtil entitiesUtil;
    @Autowired Tools tools;
    @Autowired PasswordEncoder encoder;

    List<Entity> entityArrayList = new ArrayList<>();
    ArrayList<EntityDef> entityDefs = new ArrayList<>();

    public static void main(String[] args) {
        try {
            SpringApplication.run(Application.class, args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    HashMap menus = new HashMap();
    @Value("classpath:/menu.list")
    Resource menufile;

    @Override
    public void run(String... args) throws Exception {

        Timer timer = new Timer("Timer");
        long delay = 1000L;
        long period = 3600000L;
        timer.scheduleAtFixedRate(task, delay, period);


        importAllEntity();
        importPrivileges();
        importRoles();
        menuDao.deleteAll();


        try {
            List<List<String>> menuList = readCsvToList(menufile, 0);
            for (List<String> inner : menuList) {
                if (inner.size() == 2) menus.put(inner.get(0), inner.get(1));
            }
        }catch (Exception e){
            System.out.println("Error : "+e.getMessage());
        }

        importAllMenu();

        createRootMenu();
        createStartUpUsers();

        logger.debug("application started");
    }


    private void importAllMenu() {
        ArrayList<String> root =
                new ArrayList<>(
                        Arrays.asList(
                                "Administration",
                                "Carrier",
                                "Finance",
                                "Reference books",
                                //      "Reports",
                                "Sms",
                                "Voice",
                                "Routing",
                                "WebCenter",
                                "Wholesale Portal",
                                "Retail Portal"));
        importMenu("root", root);

        ArrayList<String> Administration = new ArrayList<>(Arrays.asList(
//                "Account manager history", //Account_get_out
                "Custom parameter types",
                "Email Processing rules",//Email_rule_get_out
                "Outgoing email accounts",
                "Account Manager Mapping",
                "Incomming email accounts",
                "Report schadule",  //Report_param_get_out
                "Service notifications",
//                "System jobs",
                "Main Config",
                "Trace analyzer",
                "Templates"));
        importMenu("Administration", Administration);

        ArrayList<String> System_Settings= new ArrayList<>(Arrays.asList(
                "Common",
                "Data import",
                "Financial module",
                "Partner portal",
                //       "Report schadule",
                "SMS",
                "SMS analytics",
                "SMS rates",
                "SMS routing",
                "SMS switch",
                "SMS Test",
                "Security"
//                ,
//                "Trading tools"
        ));
        importMenu("Main Config", System_Settings);

        ArrayList<String> Template_manager = new ArrayList<>(Arrays.asList(
                "Markers",
                "Template manager2"
        ));
        importMenu("Templates", Template_manager);

        ArrayList<String> Carrier = new ArrayList<>(Arrays.asList(
                "Carrier",//Carrier_get_out
                "User", //User_get_out
                "Account",//Account_get_out
                "Agreement",//Agreement_get_out
                "Product",//Product_get_out
                "Voice POI",//Voice_poi_get_out
                "Sms channel",//Sms_channel_get_out
                "Sms POI"
//                ,//Sms_poi_get_out
//                "Settings"
        ));
        importMenu("Carrier", Carrier);

        ArrayList<String> Finance = new ArrayList<>(Arrays.asList(
                "Charges",
                "Invoice",//Invoice_get_out
                "Payment",//Payment_get_out
                "Recurring Fee"));
        importMenu("Finance", Finance);

        ArrayList<String> Reference_books = new ArrayList<>(Arrays.asList(
                "Bank Accounts",
                "Tag",//Tag_get_out
//                "Tag import",//Tag_get_out
                "Contract Company",//Contract_company_get_out
                "Currency",//Currency_get_out
                "Product Type",
                "Region",//Region_get_out
                "Units"
        ));
        importMenu("Reference books", Reference_books);

        ArrayList<String> Regions = new ArrayList<>(Arrays.asList(
                "Company Region",
                "Country Region"
        ));
        importMenu("Regions", Regions);

        ArrayList<String> Reports = new ArrayList<>(Arrays.asList(
                "Reports page overview",//Sms_poi_ema_stats_get_out
                "Change logs",//Sms_poi_ema_stats_get_out
                "System logs",//Sms_poi_ema_stats_get_out
                "REST API changes",//Sms_poi_ema_stats_get_out
                "Analytical cube status",//Sms_poi_ema_stats_get_out
//                "Voice Volume-ASR-ACD monitoring Alert",//Sms_poi_ema_stats_get_out
//                "ASR alert SMS",//Sms_poi_ema_stats_get_out
//                "Available vendor routes SMS",//Sms_poi_ema_stats_get_out
//                "Channel status SMS",//Sms_poi_ema_stats_get_out
//                "Channel status updates SMS",//Sms_poi_ema_stats_get_out
//                "EDR export SMS",//Sms_poi_ema_stats_get_out
//                "LCR analysis SMS",//Sms_poi_ema_stats_get_out
//                "MPS per vendor SMS",//Sms_poi_ema_stats_get_out
//                "Negative margin SMS Alert",//Sms_poi_ema_stats_get_out
//                "Negative margin SMS",//Sms_poi_ema_stats_get_out
//                "Rate export SMS",//Sms_poi_ema_stats_get_out
//                "Rate generator SMS",//Sms_poi_ema_stats_get_out
//                "Rule list SMS",//Sms_poi_ema_stats_get_out
//                "Routing rules with static vendor rates SMS",//Sms_poi_ema_stats_get_out
//                "Invoice generation delay Finance Voice",//Sms_poi_ema_stats_get_out
                "Report builder creating a customized report"));
        importMenu("Reports", Reports);

        ArrayList<String> SMS = new ArrayList<>(Arrays.asList(
                //         "Dashboard",//Dashboard
                "Send Sms",//Edr_get_out
                "Sms-Out",//Edr_get_out
                "Sms-In",//Edr_get_out
                "Sms Edr",//Edr_get_out
                "Sms Rate",//Export_sms_rate_get_out
                "SMS Switch Settings",//Export_sms_rate_get_out
//                "SMS Reference books2",
//                "SMS Test system",
                "SMS Volume Based Deals"
        ));
        importMenu("Sms", SMS);
        ArrayList<String> VOICE = new ArrayList<>(Arrays.asList(
                //      "Dashboard",//Dashboard
                "Voice Edr",//Edr_get_out
                "Voice Rate",//Export_sms_rate_get_out
//                "Voice Reference books2",
//                "Voice Routing",
//                "Voice Test system",
                "Voice Volume based deals"
        ));
        importMenu("Voice", VOICE);



        ArrayList<String> EDR_management = new ArrayList<>(Arrays.asList(
                "Sms EDR",//Edr_get_out
                "Reconciliation"
//                ,
//                "EDR rerating"
        ));
        importMenu("Edr", EDR_management);
        ArrayList<String> EDR_rerating = new ArrayList<>(Arrays.asList("Recalculation settings",
                "Tasks",//Task_get_out
                "Task Details"));
        importMenu("EDR rerating", EDR_rerating);


        ArrayList<String> Rates = new ArrayList<>(Arrays.asList(
                "Auto Rate Import"
                ,"Rate Groups"
                ,"Sms Rate"//Sms_rate_get_out
                ,"Voice Rate"//Voice_rate_get_out
                ,"Rate Compilation"
                ,"Rate Sheet Parsing"
                ,"Rate Management Tools"));
        importMenu("Rates", Rates);


        ArrayList<String> Reference_books2 = new ArrayList<>(Arrays.asList("Short code reference book editor",
                "E212-E164 reference book editor",
                "E212-E164 reference book import",
                "Billing status presets"));
        importMenu("Reference books2", Reference_books2);

        ArrayList<String> Routing = new ArrayList<>(Arrays.asList(
                "Routing Rules",
//                ,
                "Dynamic Options",
                "Routing Formula Template"
//                "Routing Interface",
//                "Routing rules page",
//                "Statistical parameters in routing"
//                ,"Routing configuration algorithm",
//                "Simulation","Routing features",
//                "Routing statistics",
//                "Translation rules"
        ));
        importMenu("Routing", Routing);
        ArrayList<String> Simulation = new ArrayList<>(Arrays.asList("Simulation log","Send SMS"));
        importMenu("Simulation", Simulation);


        ArrayList<String> Test_system = new ArrayList<>(Arrays.asList(
                //   "Test destinations",
                "Task settings"
                //,
                //       "Test tasks",
                //       "Using TestMySMS to test SMS delivery",
                //      "Using remote365 to test SMS delivery"
                //      ,"Using TelQ to test SMS delivery",
                //      "Using CSG Assure service to test SMS delivery"
        ));
        importMenu("Test system", Test_system);


        ArrayList<String> WebCenter = new ArrayList<>(Arrays.asList("SMS pack",
                "SMS pack user subscription"));
        importMenu("WebCenter", WebCenter);




        ArrayList<String> Wholesale_portal = new ArrayList<>(Arrays.asList("Invoices",
                "Payments",
                "SMS Stats",//Sms_rate_get_out
                "SMS rates",//Sms_poi_ema_stats_get_out
                "SMS POIs",//Sms_poi_get_out
                "EDRs"));
        importMenu("Wholesale Portal", Wholesale_portal);

        ArrayList<String> Retail_portal = new ArrayList<>(Arrays.asList("Providing access",
                //      "Dashboard",
                "Sms campaign",//Sms_campaign_get_out
                //   "Statistics",
                "Contract company",//Contract_company_get_out
                "Black list",//Black_list_get_out
                //     "Templates",
                "Purchase",
                "Connections"
                //    "Account settings",
                //  "Administration2")
        ));
        importMenu("Retail Portal", Retail_portal);




    }

    public List<List<String>> readCsvToList(Resource pathToCsv, int logger) {
        List<List<String>> ret = new ArrayList<>();
        try {
            InputStream carrierListContent = menufile.getInputStream();

            Reader carrierContentReader =
                    new InputStreamReader(carrierListContent, StandardCharsets.UTF_8);
            //    File csvFile = ResourceUtils.getFile(pathToCsv);
            //    if (csvFile.isFile()) {
            BufferedReader csvReader = new BufferedReader(carrierContentReader);
            String row;
            while ((row = csvReader.readLine()) != null) {
                if (logger != 0) System.out.println(row);
                String[] data = row.split(",");
                ret.add(Arrays.asList(data));
                // do something with the data
            }
            csvReader.close();
            //  } else System.out.println("FIle Not Found .");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    private void importAllEntity() {
        try {
            InputStream EntityListContent = entityFile.getInputStream();
            Reader EntityContentReader = new InputStreamReader(EntityListContent, StandardCharsets.UTF_8);
            Entity[] entities = new Gson().fromJson(EntityContentReader, Entity[].class);
            entityDefs = new ArrayList<>();
            for (Entity entity : entities) {
                this.entityArrayList.add(entity);
                ArrayList<FieldDef> fields = new ArrayList<>();
                for (EntityField ef : entity.getFields()) {
                    FieldDef fd = new FieldDef();
                    fd.setFriendlyName(ef.fieldName);
                    fd.setFieldName(ef.fieldName);
                    fd.setType(ef.fieldDataType);
                    fields.add(fd);
                }
                EntityDef entityDef = new EntityDef();
                entityDef.setId(String.valueOf(UUID.randomUUID()));
                entityDef.setFriendlyName(entity.getEntityName());
                entityDef.setName(entity.getEntityName());
                entityDef.setFields(fields);
                entityDef.setEndPoint("/" + tools.toDashLimitedSingular(entity.getEntityName()));

                ArrayList<EntityConf> entityConfs = new ArrayList<>();
                EntityConf entityConf = new EntityConf();
                entityConf.setName("add");
                entityConf.setValue("true");
                entityConfs.add(entityConf);

                entityConf = new EntityConf();
                entityConf.setName("edit");
                entityConf.setValue("true");
                entityConfs.add(entityConf);

                entityConf = new EntityConf();
                entityConf.setName("delete");
                entityConf.setValue("true");
                entityConfs.add(entityConf);

                entityConf = new EntityConf();
                entityConf.setName("search");
                entityConf.setValue("true");
                entityConfs.add(entityConf);

                entityConf = new EntityConf();
                entityConf.setName("export");
                entityConf.setValue("true");
                entityConfs.add(entityConf);

                entityConf = new EntityConf();
                entityConf.setName("import");
                entityConf.setValue("true");
                entityConfs.add(entityConf);

                entityConf = new EntityConf();
                entityConf.setName("views");
                entityConf.setValue("true");
                entityConfs.add(entityConf);

                entityDef.setSetting(entityConfs);

                entityDefs.add(entityDef);
                entitiesUtil.entityMap.put(entityDef.getFriendlyName(), entityDef);
                if(entity.hasDB)entitiesUtil.entityMapEntity.put(entity.entityName, entity);
            }
            entityDefDao.deleteAll();
            entityDefDao.create(entityDefs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void importPrivileges() {
        privilegeDao.deleteAll();
        for (Entity name : entityArrayList) {
            try {
                Privilege a = new Privilege();
                a.setFriendlyName(name.entityName.toUpperCase() + "_READ_PRIVILEGE");
                a.setId(name.entityName.toUpperCase() + "_READ_PRIVILEGE");
                Privilege b = new Privilege();
                b.setFriendlyName(name.entityName.toUpperCase() + "_WRITE_PRIVILEGE");
                b.setId(name.entityName.toUpperCase() + "_WRITE_PRIVILEGE");
                privilegeDao.create(a);
                privilegeDao.create(b);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void importRoles() throws Exception {
        if (roleDao.findBy("FriendlyName", "ADMIN_ROLE") == null) {
            Role adminRole = new Role();
            adminRole.setId("ADMIN_ROLE");
            adminRole.setFriendlyName("ADMIN_ROLE");
            ArrayList<Privilege> pr =
                    (ArrayList<Privilege>) privilegeDao.search(tools.getSearchCriteria(null), "id", "ASC","", new AtomicReference<Boolean>(true), false);
            adminRole.setPrivileges(pr);
            roleDao.create(adminRole);
        }
        if (roleDao.findBy("FriendlyName", "READ_ONLY_ROLE") == null) {
            Role roRole = new Role();
            roRole.setId("READ_ONLY_ROLE");
            roRole.setFriendlyName("READ_ONLY_ROLE");
            ArrayList<Privilege> prs =
                    (ArrayList<Privilege>) privilegeDao.search(tools.getSearchCriteria(null), "id", "ASC","", new AtomicReference<Boolean>(true), false);
            ArrayList<Privilege> prsFinal = new ArrayList<>();
            for (Privilege pr : prs) {
                if (pr.getFriendlyName().contains("_READ_PRIVILEGE")) {
                    prsFinal.add(pr);
                }
            }
            Privilege viewPrivileges = privilegeDao.findBy("FriendlyName", "VIEW_WRITE_PRIVILEGE");
            if (viewPrivileges != null) {
                prsFinal.add(viewPrivileges);
            }
            roRole.setPrivileges(prsFinal);
            roleDao.create(roRole);
        }
        if (roleDao.findBy("FriendlyName", "FINANCIAL_ROLE") == null) {
            Role fRole = new Role();
            fRole.setId("FINANCIAL_ROLE");
            fRole.setFriendlyName("FINANCIAL_ROLE");
            ArrayList<Privilege> prsFinal = new ArrayList<>();
            ArrayList<String> fRoles =
                    new ArrayList<>(
                            Arrays.asList(
                                    "USER_READ_PRIVILEGE",
                                    "VOUCHER_READ_PRIVILEGE",
                                    "VOUCHER_WRITE_PRIVILEGE",
                                    "ENTITYDEF_READ_PRIVILEGE",
                                    "VIEW_READ_PRIVILEGE",
                                    "VIEW_WRITE_PRIVILEGE",
                                    "MENU_READ_PRIVILEGE",
                                    "INVOICE_READ_PRIVILEGE",
                                    "INVOICE_WRITE_PRIVILEGE"));
            for (String prName : fRoles) {
                Privilege pr = privilegeDao.findBy("FriendlyName", prName);
                if (pr != null) {
                    prsFinal.add(pr);
                }
            }
            fRole.setPrivileges(prsFinal);
            roleDao.create(fRole);
        }
    }

    public void createStartUpUsers() {
        User adminAccount = userDao.findBy("username", "admin");
        if (adminAccount != null) {
            userDao.delete(adminAccount.getId());
        }
        adminAccount = new User();
        adminAccount.setUserName("admin");
        Role role = roleDao.findBy("FriendlyName", "ADMIN_ROLE");
        ArrayList<String> roles = new ArrayList<>();
        roles.add(role.getId());
        adminAccount.setFriendlyName("admin");
        adminAccount.setRoles(roles);
        adminAccount.setCredit(1000.0);
        adminAccount.setPassword(encoder.encode("password"));
        adminAccount.setId("admin");
        userDao.create(adminAccount);
    }

    private void importMenu(String parentId, ArrayList<String> root) {

        int order = 1;
        if (!parentId.equals("root")) {
            try {
                parentId = menuDao.findBy("FriendlyName", parentId).getId();
            }catch (Exception e){
//                System.out.println("parentId = " + parentId);
            }
        }
        for (String item : root) {
            //   item = WordUtils.capitalizeFully(item).replaceAll(" ","");
            Menu menu = new Menu();
            menu.setCaption(item);
            menu.setFriendlyName(item);
            menu.setDateAdded(new Date(System.currentTimeMillis()));
            menu.setEnabled(true);
            if(menus.get(item)!=null ){
                if(((String)menus.get(item)).length()>0)menu.setEndPoint((String) menus.get(item));
                else {
//                    if(findUsingEnhancedForLoop(item)==null) System.out.println("item = " + item);
                    menu.setEndPoint(item.replaceAll(" ",""));
                }
            }
            else {
                System.out.println("item = " + item);
                menu.setEndPoint(item.replaceAll(" ",""));
            }
            EntityDef dummyEntityDef = new EntityDef();
            dummyEntityDef.setName(item);
            dummyEntityDef.setFriendlyName(item);
            EntityDef ans = entitiesUtil.entityMap.get(item) != null
                    ? entitiesUtil.entityMap.get(item)
                    : dummyEntityDef;

            if(!parentId.equals("root")){
                menu.setMenuType(MenuType.Entity);
            }else{
                menu.setMenuType(MenuType.Label);
            }

            // ans.setEndpoint((String) menus.get(item));
            if(menus.get(item)!=null ){
                if(((String)menus.get(item)).length()>0)ans.setEndPoint((String) menus.get(item));
                else {
                    if(findUsingEnhancedForLoop(item.replaceAll(" ",""))==null) System.out.println("itema = " + item);
                    ans.setEndPoint(item.replaceAll(" ",""));
                }
            }
            else ans.setEndPoint(item.replaceAll(" ",""));
            menu.setEntity(ans);
            menu.setEndPoint(ans.getEndPoint());
            menu.setIcon("https://simpleapp.calligo.dev/assets/img/sidebar/billing.png");
            menu.setOrder(order);
            order++;
            menu.setParentId(parentId);
            menu.setOwnerId("SYSTEM");
            String Id = String.valueOf(UUID.randomUUID());
            menu.setId(Id);
            menuDao.create(menu);
        }
    }

    private void createRootMenu() {
        Menu rootMenu;
        rootMenu = menuDao.findBy("_id", "root");
        if (rootMenu == null) {
            rootMenu = new Menu();
            rootMenu.setCaption("root");
            rootMenu.setDateAdded(new Date(System.currentTimeMillis()));
            rootMenu.setEnabled(true);
            rootMenu.setEntity(null);
            rootMenu.setId("root");
            rootMenu.setOrder(0);
            rootMenu.setParentId(null);
            menuDao.create(rootMenu);
        }

        ArrayList<String> root =
                new ArrayList<>(
                        Arrays.asList(
                                "Administration",
                                "Carrier",
                                "Finance",
                                "Reference books",
                                //      "Reports",
                                "Sms",
                                "Voice",
                                "Routing",
                                "WebCenter",
                                "Wholesale Portal",
                                "Retail Portal"));
        int order = root.size();
        Menu menu = new Menu();
        menu.setMenuType(MenuType.Url);
        menu.setCaption("Invoice Generation");
        menu.setFriendlyName("Invoice Generation");
        menu.setUrl("custom-pages/invoices");
        menu.setDateAdded(new Date(System.currentTimeMillis()));
        menu.setEnabled(true);
        menu.setEntity(new EntityDef());
        menu.setEndPoint("");
        menu.setIcon("https://simpleapp.calligo.dev/assets/img/sidebar/billing.png");
        menu.setOrder(order);
        order++;
        menu.setParentId("root");
        menu.setOwnerId("SYSTEM");
        String Id = String.valueOf(UUID.randomUUID());
        menu.setId(Id);
        menuDao.create(menu);

        menu = new Menu();
        menu.setMenuType(MenuType.Url);
        menu.setCaption("Dashboards");
        menu.setFriendlyName("Dashboards");
        menu.setUrl("neo-page/list");
        menu.setDateAdded(new Date(System.currentTimeMillis()));
        menu.setEnabled(true);
        menu.setEntity(new EntityDef());
        menu.setEndPoint("");
        menu.setIcon("https://simpleapp.calligo.dev/assets/img/sidebar/billing.png");
        menu.setOrder(order);
        order++;
        menu.setParentId("root");
        menu.setOwnerId("SYSTEM");
        Id = String.valueOf(UUID.randomUUID());
        menu.setId(Id);
        menuDao.create(menu);

        menu = new Menu();
        menu.setMenuType(MenuType.Url);
        menu.setCaption("New Dashboard");
        menu.setFriendlyName("New Dashboard");
        menu.setUrl("neo-page/builder");
        menu.setDateAdded(new Date(System.currentTimeMillis()));
        menu.setEnabled(true);
        menu.setEntity(new EntityDef());
        menu.setEndPoint("");
        menu.setIcon("https://simpleapp.calligo.dev/assets/img/sidebar/billing.png");
        menu.setOrder(order);
        order++;
        menu.setParentId("root");
        menu.setOwnerId("SYSTEM");
        Id = String.valueOf(UUID.randomUUID());
        menu.setId(Id);
        menuDao.create(menu);

    }

    public String findUsingEnhancedForLoop(
            String name) {

        for (EntityDef customer : entityDefs) {
            if (customer.getName().equals(name)) {
                return customer.getName();
            }
        }
        return null;
    }


    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }

    TimerTask task = new TimerTask() {
        public void run() {
            JsoupProvider.schedule();
        }
    };












}
