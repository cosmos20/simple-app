package dev.calligo.simpleapp.service;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSBuckets;
import com.mongodb.client.gridfs.GridFSDownloadStream;
import com.mongodb.client.gridfs.GridFSFindIterable;
import com.mongodb.client.gridfs.model.GridFSFile;

import dev.calligo.simpleapp.models.generated.FileDownload;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import io.undertow.util.FileUtils;
import org.apache.poi.util.IOUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.springframework.data.mongodb.core.query.Query.query;
import static org.springframework.data.mongodb.gridfs.GridFsCriteria.whereFilename;
import java.io.IOException;

@Service
public class FileStorageService {

  @Autowired private GridFsTemplate gridFsTemplate;

  @Autowired private GridFsOperations operations;
  @Autowired private MongoClient MongoClient;

  private GridFSBucket gridFSBucket;

  public String addVideo(MultipartFile file) throws IOException {
    DBObject metaData = new BasicDBObject();
    metaData.put("type", file.getContentType());
    metaData.put("title", file.getOriginalFilename());
    ObjectId id =
        gridFsTemplate.store(
            file.getInputStream(), file.getOriginalFilename(), file.getContentType(), metaData);
    return id.toString();
  }

  public FileDownload getVideo(String id) throws IllegalStateException, IOException {
    GridFSFile file = gridFsTemplate.findOne(new Query(Criteria.where("_id").is(id)));

    //      GridFSFindIterable result = operations.find(query(whereFilename().is(id)));
    //      GridFSFile gridFSFile = result.first();

    MongoDatabase myDatabase = MongoClient.getDatabase("kofitness");
    gridFSBucket =
        (GridFSBucket) GridFSBuckets.create(myDatabase); // .openDownloadStream(file.getObjectId());
    GridFSDownloadStream gridFSDownloadStream = gridFSBucket.openDownloadStream(file.getObjectId());
    // Create gridFsResource, for obtaining the stream object
    GridFsResource gridFsResource = new GridFsResource(file, gridFSDownloadStream);
    byte[] bytes = IOUtils.toByteArray(gridFsResource.getInputStream());

    FileDownload video = new FileDownload();
    video.setFileName(file.getMetadata().get("title").toString());
    video.setId(id);
    //    video.setStream(operations.getResource(file). .getInputStream());
    video.setFiledAt(bytes);
    return video;
  }
}
