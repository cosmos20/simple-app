#!/usr/bin/env bash

export AWS_DEFAULT_OUTPUT="table"
export TASK_DEFINITION_REV="1"
export AWS_PAGER=""
export GITLAB_TOKEN=""
tg_arn=""
rule_arn=""
priority="-1"
definition="1"

function install_aws_cli() {
    echo "== installing aws cli on ${OSTYPE}"
    if [[ "${OSTYPE}" == "linux-gnu"* ]]; then
            curl -s "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
            unzip awscliv2.zip && \
            sudo ./aws/install --update
    elif [[ "${OSTYPE}" == "darwin"* ]]; then
            curl -s "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg" && \
            sudo installer -pkg AWSCLIV2.pkg -target /
    else
            echo "OS unknown"
            exit 1
    fi
}

function install_jq() {
    echo "== installing jq on ${OSTYPE}"
    if [[ "${OSTYPE}" == "linux-gnu"* ]]; then
            sudo apt install jq -y
    elif [[ "${OSTYPE}" == "darwin"* ]]; then
            brew install jq
    else
            echo "OS unknown"
            exit 1
    fi
}

function check_aws_cli() {
  echo "== checking if AWS CLI exist"
  if [[ "$(aws --version)" =~ aws-cli* ]]; then
    return 0
  else
    return 1
  fi
}

function check_jq() {
  echo "== checking if jq exist"
  which "jq"
  return $?
}

function cleanup() {
    echo "== cleaning up"
    rm -rf awscliv2.zip 2>/dev/null && \
    rm -rf aws 2>/dev/null && \
    rm -f temp.json 2>/dev/null
    rm -f AWSCLIV2.pkg 2>/dev/null
}

function create_ecr() {
  echo "== creating cosmos20/simpleapp-api registry in ECR"
  aws ecr describe-repositories --repository-names cosmos20/simpleapp-api --no-paginate || aws ecr create-repository --repository-name cosmos20/simpleapp-api --no-paginate
}

function get_aws_credentials(){
  echo "ENTER AWS_ACCESS_KEY_ID: "
  read -r -s ACCESS_KEY_ID
  export AWS_ACCESS_KEY_ID=${ACCESS_KEY_ID}

  echo "ENTER AWS_SECRET_ACCESS_KEY: "
  read -r -s SECRET_ACCESS_KEY
  export AWS_SECRET_ACCESS_KEY=${SECRET_ACCESS_KEY}

  echo "ENTER AWS REGION: "
  read -r REGION
  export AWS_DEFAULT_REGION=${REGION}

  mkdir ~/.aws 2>/dev/null

  cat <<EOF > ~/.aws/credentials
[default]
aws_access_key_id=${AWS_ACCESS_KEY_ID}
aws_secret_access_key=${AWS_SECRET_ACCESS_KEY}
EOF
  cat <<EOF > ~/.aws/config
[default]
region=${REGION}
EOF
}

function load_aws_credentials() {
  echo "== loading aws credentials"
  export AWS_ACCESS_KEY_ID=$(cat ~/.aws/credentials | grep aws_access_key_id | cut -d'=' -f2 | tr -d ' ')
  export AWS_SECRET_ACCESS_KEY=$(cat ~/.aws/credentials | grep aws_secret_access_key | cut -d'=' -f2 | tr -d ' ')
  export AWS_DEFAULT_REGION=$(cat ~/.aws/config | grep region | cut -d'=' -f2 | tr -d ' ')
}

function get_gitlab_credentials(){
  echo "ENTER GITLAB PRIVATE TOKEN: "
  read -r -s GTOKEN
  export GITLAB_TOKEN=${GTOKEN}
}

function check_cluster() {
    echo "== checking if cluster calligo exist"
    CLUSTER_LIST=$(aws ecs list-clusters --no-paginate)
    if [[ ${CLUSTER_LIST} =~ cluster/calligo ]];then
      return 0
    else
        echo ">>> cluster doesn't exist, please create it manually <<<"
        return 1
    fi
}

function create_task_definition() {
    echo "== creating task definition"
    aws ecs register-task-definition --region us-east-1 --family simpleapp-api --output json --cli-input-json file://_deploy/ecs_deploy_init.json > temp.json
    if [[ -f temp.json ]];then
        definition=`grep  '"revision"' temp.json | awk '{print $2}' | cut -d',' -f1`
    fi
}

function create_dns_record(){
  echo "== creating dns record"
  aws route53 change-resource-record-sets --hosted-zone-id Z0332507E0IGPCHVWVKU \
  --change-batch '{ "Comment": "record for simpleapp service",
  "Changes": [ { "Action": "UPSERT", "ResourceRecordSet": { "Name":
  "api-simpleapp.calligo.dev", "Type": "A",
        "AliasTarget":{
            "HostedZoneId": "Z35SXDOTRQ7X7K",
            "DNSName": "dualstack.smshub-1447571642.us-east-1.elb.amazonaws.com",
            "EvaluateTargetHealth": false
                    }
            } }
            ]
    }' --no-paginate
}

function check_aws_credentials() {
  echo "== checking aws credentials"
  aws --region us-east-1 sts get-caller-identity  --endpoint-url https://sts.us-east-1.amazonaws.com --no-paginate
}

function create_target_group() {
  echo "== creating target group"
  aws elbv2 describe-target-groups --name simpleapp-api || \
  aws elbv2 create-target-group \
    --name simpleapp-api\
    --protocol HTTP \
    --port 8080 \
    --target-type ip \
    --vpc-id vpc-03db3156835041491 \
    --health-check-path /actuator/health \
    --health-check-port 8080 \
    --health-check-interval-seconds 60 \
    --health-check-timeout-seconds 10 \
    --healthy-threshold-count 2 \
    --unhealthy-threshold-count 5
  tg_arn=$(aws elbv2 describe-target-groups --names simpleapp-api | grep 'TargetGroupArn' | cut -d '|' -f 4 | tr -d ' ')
}

function create_service() {
  echo "== creating service"
      aws ecs create-service \
        --cluster calligo  \
        --service-name simpleapp-api \
        --task-definition simpleapp-api:${definition} \
        --desired-count 1 \
        --launch-type FARGATE \
        --platform-version LATEST \
        --load-balancers "targetGroupArn=${tg_arn},containerName=simpleapp-api,containerPort=8080" \
        --scheduling-strategy REPLICA \
        --network-configuration "awsvpcConfiguration={subnets=[subnet-0ec497cb992c2c7b6,subnet-08fadf345b1e924ce],securityGroups=[sg-0e34fa05003e5d40b],assignPublicIp=DISABLED}"
}

function create_elb_rule(){
  echo "== creating elb rule"
  current_rules=$(aws elbv2 describe-rules \
    --listener-arn arn:aws:elasticloadbalancing:us-east-1:487932330115:listener/app/smshub/9497af0ec2876ba3/4db8884fb46feff9 \
    --output json | \
    grep -w ${tg_arn})
  if [[ -z ${current_rules} ]];then
      rule_arn=$(aws elbv2 create-rule \
      --listener-arn arn:aws:elasticloadbalancing:us-east-1:487932330115:listener/app/smshub/9497af0ec2876ba3/4db8884fb46feff9 \
      --conditions "[{\"Field\": \"host-header\", \"Values\" : [\"api-simpleapp.calligo.dev\"]}]" \
      --actions "[{\"Type\": \"forward\", \"TargetGroupArn\": \"${tg_arn}\"}]" \
      --priority ${priority} \
      --tags "[{\"Key\": \"service_name\", \"Value\": \"simpleapp-api\"}]" 2>&1)
      echo ${rule_arn} | perl -pe 's/.*(arn:aws.*listener.*?)\s+\|.*/$1/' > rule_arn.tmp
  fi
}

function get_priority(){
  echo "== get priority"
  current_priority=$(aws elbv2 describe-rules \
    --listener-arn arn:aws:elasticloadbalancing:us-east-1:487932330115:listener/app/smshub/9497af0ec2876ba3/4db8884fb46feff9 \
    --output json | \
    grep Priority | \
    cut -d '"' -f4 | \
    sort -n | \
    tail -1
  )
  priority=$(echo ${current_priority} + 1 | bc)
}

function create_git_project(){
  echo "== creating git project"
  git_id=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X POST "https://gitlab.com/api/v4/projects?name=simpleapp-api&issues_enabled=false" | jq '.id')
  if ! [[ -n ${id} ]];then
    resp=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X POST "https://gitlab.com/api/v4/projects?name=simpleapp-api&issues_enabled=false" | jq '.message.name[0]')
    if ! [[ ${resp} =~ already ]];then
      echo "git project creation failed"
      exit 1
    else
      git_id=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" -X GET "https://gitlab.com/api/v4/projects/cosmos20%2Fsimpleapp-api" | jq .'id')
    fi
  fi
  echo "git project id is ${git_id}"
  curl --request POST -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${git_id}/variables" --form "key=AWS_ACCESS_KEY_ID" --form "value=${AWS_ACCESS_KEY_ID}"
  sleep 1
  curl --request POST -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${git_id}/variables" --form "key=AWS_SECRET_ACCESS_KEY" --form "value=${AWS_SECRET_ACCESS_KEY}"
  sleep 1
  curl --request POST -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${git_id}/variables" --form "key=AWS_DEFAULT_REGION" --form "value=us-east-1"
}

function git_init(){
  echo "== initializing git"
    if [[ -d .git ]];then
      git add .
      git commit -am "Committed at $(date +%Y%m%d%H%M%S) from $(hostname)"
      git push -u origin master
    else
        git init
        git remote add origin https://gitlab.com/cosmos20/simpleapp-api.git
        git add .
        git commit -m "Initial commit"
        git push -u origin master
    fi
}


# check credentials
check_aws_credentials || \
# get aws credentials
get_aws_credentials && \
# load aws credentials
load_aws_credentials &&
# check aws cli
check_aws_cli || \
# install aws cli
install_aws_cli && \
# check jq
check_jq || \
# install aws cli
install_jq && \
# get gitlab credentials
get_gitlab_credentials && \
# create registry (ecr)
create_ecr && \
# check cluster (ecs)
check_cluster && \
# create task definition (ecs)
create_task_definition && \
# create dns record to resource(alb)
create_dns_record && \
# create target group
create_target_group && \
# get priority
get_priority && \
# create elb rule
create_elb_rule && \
# create service
create_service
cleanup
# create git project and variables
create_git_project
# git init and push
git_init

