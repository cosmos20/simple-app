package dev.calligo.simpleapp.services.generated;

import dev.calligo.simpleapp.models.generated.Privilege;
import dev.calligo.simpleapp.models.generated.Role;
import dev.calligo.simpleapp.utils.Tools;
import dev.calligo.simpleapp.search.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class JwtUserDetailsService implements UserDetailsService {
  @Autowired UserDao userDao;
  @Autowired PrivilegeDao privilegeDao;
  @Autowired RoleDao roleDao;
  @Autowired Tools tools;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    if ("admin".equals(username)) {
      List<GrantedAuthority> authorities = new ArrayList<>();
      try {
        for (Privilege privilege :
            privilegeDao.search(
                tools.getSearchCriteria(null),
                "id",
                "ASC",
                "",
                new AtomicReference<Boolean>(true),
                false)) {
          authorities.add(new SimpleGrantedAuthority(privilege.getFriendlyName()));
        }
      } catch (Exception e) {
        e.printStackTrace();
      }

      return new User(
          "admin", "$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6", authorities);
    } else {
      try {
        dev.calligo.simpleapp.models.generated.User user = userDao.findBy("username", username);
        String loginName = user.getUserName();
        String password = user.getPassword();

        List<GrantedAuthority> authorities = new ArrayList<>();

        List<Privilege> privileges = new ArrayList<>();

        for (String roleId : user.getRoles()) {
          try {
            Privilege r = privilegeDao.findBy("id", roleId);
            if (r != null) {
              privileges.add(r);
            }
          } catch (Exception e) {
            e.printStackTrace();
          }
          Role r = roleDao.findBy("id", roleId);
          if (r != null) {
            r.getPrivileges()
                .forEach(
                    s -> {
                      privileges.add(s);
                    });
          }
        }
        for (Privilege privilege : privileges) {
          authorities.add(new SimpleGrantedAuthority(privilege.getFriendlyName()));
        }
        return new User(loginName, password, authorities);

      } catch (Exception e) {
        throw new UsernameNotFoundException("User not found with loginName: " + username);
      }
    }
  }
}
