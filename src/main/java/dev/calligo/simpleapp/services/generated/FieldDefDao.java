package dev.calligo.simpleapp.services.generated;

import dev.calligo.simpleapp.models.generated.*;
import dev.calligo.simpleapp.models.generated.FieldDef;
import dev.calligo.simpleapp.enums.generated.*;
import dev.calligo.simpleapp.search.SearchCriteria;
import org.springframework.data.domain.Page;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public interface FieldDefDao {

  /*
   create single FieldDef
  */
  FieldDef create(FieldDef FieldDef);

  /*
   create multiple FieldDefs
  */
  List<FieldDef> create(List<FieldDef> FieldDefs);

  /*
   update single FieldDef
  */
  FieldDef update(FieldDef FieldDef);

  /*
   delete single FieldDef
  */
  void delete(String id);

  /*
   delete multiple FieldDefs
  */
  void deleteMany(List<String> ids);

  /*
   delete all FieldDefs
  */
  void deleteAll();

  /*
   find user by id
  */
  FieldDef find(
      String Id, boolean cache, String userid, AtomicReference<Boolean> isAdmin, Boolean hasfield);

  /*
   search for FieldDefs using search criteria
  */
  Page<FieldDef> search(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      int page,
      int size,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  long count(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      int size,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  /*
   search for FieldDefs using search criteria - non paginated
  */
  List<FieldDef> search(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  /*
   find by field
  */
  FieldDef findBy(String fieldName, String value);

  List<FieldDef> getall(ArrayList<String> fieldNames);
}
