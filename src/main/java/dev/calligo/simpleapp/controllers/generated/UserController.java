package dev.calligo.simpleapp.controllers.generated;

import dev.calligo.simpleapp.exceptions.global.ConflictException;
import dev.calligo.simpleapp.exceptions.global.InternalException;
import dev.calligo.simpleapp.exceptions.global.InvalidRequestException;
import dev.calligo.simpleapp.exceptions.global.NotFoundException;
import dev.calligo.simpleapp.utils.EntitiesUtil;
import dev.calligo.simpleapp.enums.generated.*;
import dev.calligo.simpleapp.converters.generated.*;
import dev.calligo.simpleapp.models.generated.User;
import dev.calligo.simpleapp.search.SearchCriteria;
import dev.calligo.simpleapp.security.AuthenticationFacade;
import dev.calligo.simpleapp.services.generated.*;
import dev.calligo.simpleapp.utils.Exporter;
import dev.calligo.simpleapp.utils.Tools;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import dev.calligo.simpleapp.models.generated.*;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.beans.factory.annotation.Value;
import java.io.*;
import org.springframework.data.domain.Page;
import java.util.concurrent.atomic.AtomicReference;
import java.math.BigDecimal;

import javax.validation.Valid;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

// GENERATED IMPORTS

@RestController
@RequestMapping(value = "/User", produces = "application/json")
public class UserController {

  @Autowired UserDao api;
  @Autowired AuthenticationFacade authenticationFacade;
  @Autowired PasswordEncoder encoder;
  @Autowired EntitiesUtil entitiesUtil;
  @Autowired Exporter exporter;
  @Autowired Tools tools;
  @Autowired UserConverter converter;

  @RequestMapping(method = RequestMethod.GET)
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  @PreAuthorize("hasAuthority('USER_READ_PRIVILEGE')")
  public ResponseEntity<?> getUsers(
      @RequestParam(value = "orderBy", required = false, defaultValue = "id") String orderBy,
      @RequestParam(value = "direction", required = false, defaultValue = "ASC") String direction,
      @RequestParam(value = "page", required = false, defaultValue = "0") int page,
      @RequestParam(value = "size", required = false, defaultValue = "20") int size,
      @RequestParam(value = "paginate", required = false, defaultValue = "true") boolean paginate,
      @RequestParam(value = "search", required = false) String search)
      throws Exception {
    Authentication authentication = authenticationFacade.getAuthentication();
    String userid = authentication.getName();
    AtomicReference<Boolean> isAdmin = new AtomicReference<>(false);
    authentication
        .getAuthorities()
        .forEach(
            s -> {
              // if (s.getAuthority().toLowerCase().contains("write") &&
              // s.getAuthority().toLowerCase().contains("User")) isAdmin.set(true);
              if (userid.equals("admin")) isAdmin.set(true);
            });

    List<SearchCriteria> params = tools.getSearchCriteria(search);
    if (!paginate) {
      return ResponseEntity.ok()
          .body(
              api.search(params, orderBy, direction, 1, 1000, userid, isAdmin, false).getContent());
    }
    Page<User> ret = api.search(params, orderBy, direction, page, size, userid, isAdmin, false);
    return ResponseEntity.ok().body(ret);
  }

  @GetMapping(path = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @PreAuthorize("hasAuthority('USER_READ_PRIVILEGE')")
  public ResponseEntity<User> getUser(@PathVariable String id) {
    Authentication authentication = authenticationFacade.getAuthentication();
    String userid = authentication.getName();
    AtomicReference<Boolean> isAdmin = new AtomicReference<>(false);
    authentication
        .getAuthorities()
        .forEach(
            s -> {
              //   if (s.getAuthority().toLowerCase().contains("write") &&
              // s.getAuthority().toLowerCase().contains("User")) isAdmin.set(true);
              if (userid.equals("admin")) isAdmin.set(true);
            });
    User item = api.find(id, true, userid, isAdmin, false);
    if (item == null) {
      throw new NotFoundException("id not found");
    }
    return ResponseEntity.ok().body(item);
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PreAuthorize("hasAuthority('USER_WRITE_PRIVILEGE')")
  public ResponseEntity<?> deleteUser(@PathVariable String id) {
    Authentication authentication = authenticationFacade.getAuthentication();
    String userid = authentication.getName();
    AtomicReference<Boolean> isAdmin = new AtomicReference<>(false);
    authentication
        .getAuthorities()
        .forEach(
            s -> {
              //     if (s.getAuthority().toLowerCase().contains("write") &&
              // s.getAuthority().toLowerCase().contains("User")) isAdmin.set(true);
              if (userid.equals("admin")) isAdmin.set(true);
            });

    if (api.find(id, false, userid, isAdmin, false) == null) {

      throw new NotFoundException("id not found");
    }

    api.delete(id);

    //  return ResponseEntity.ok(new ApiResponse(true, "User deleted successfully@"));
    return ResponseEntity.ok().body("Deleted");
  }

  @DeleteMapping("/ids")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PreAuthorize("hasAuthority('USER_WRITE_PRIVILEGE')")
  public ResponseEntity<?> deleteUsers(@RequestParam List<String> ids) {
    api.deleteMany(ids);
    return ResponseEntity.noContent().build();
  }

  @RequestMapping(
      value = "export",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_PDF_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE})
  @ResponseStatus(HttpStatus.OK)
  @PreAuthorize("hasAuthority('USER_READ_PRIVILEGE')")
  public ResponseEntity<InputStreamResource> exportUsers(
      @RequestParam(value = "search", required = false) String search,
      @RequestParam(value = "orderBy", required = false, defaultValue = "id") String orderBy,
      @RequestParam(value = "direction", required = false, defaultValue = "ASC") String direction,
      @RequestParam(value = "fields", required = false) String fields,
      @RequestParam(value = "type") String type)
      throws Exception {
    Authentication authentication = authenticationFacade.getAuthentication();
    String userid = authentication.getName();
    AtomicReference<Boolean> isAdmin = new AtomicReference<>(false);
    authentication
        .getAuthorities()
        .forEach(
            s -> {
              // if (s.getAuthority().toLowerCase().contains("write") &&
              // s.getAuthority().toLowerCase().contains("User")) isAdmin.set(true);
              if (userid.equals("admin")) isAdmin.set(true);
            });

    List<SearchCriteria> params = tools.getSearchCriteria(search);
    List<User> Users =
        api.search(params, orderBy, direction, 1, 1000, userid, isAdmin, false).getContent();
    ByteArrayInputStream bis;
    if (Users.size() == 0) {
      bis = new ByteArrayInputStream(new byte[0]);
    } else {
      bis = exporter.export(new ArrayList<>(Users), fields, type);
    }

    HttpHeaders headers = new HttpHeaders();
    switch (type.toLowerCase()) {
      case "pdf":
        headers.add("Content-Disposition", "inline; filename=UsersExport.pdf");
        return ResponseEntity.ok()
            .headers(headers)
            .contentType(MediaType.APPLICATION_PDF)
            .body(new InputStreamResource(bis));
      case "excel":
        headers.add("Content-Disposition", "inline; filename=UsersExport.xls");
        return ResponseEntity.ok()
            .headers(headers)
            .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
            .body(new InputStreamResource(bis));
      default:
        throw new InvalidRequestException("invalid type");
    }
  }

  @GetMapping(value = "/getTemplate")
  @PreAuthorize("hasAuthority('USER_WRITE_PRIVILEGE')")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<?> getUserTemplate() throws Exception {
    HttpHeaders headers = new HttpHeaders();
    headers.add("Content-Disposition", "inline; filename=UserTemplate.csv");
    return ResponseEntity.ok()
        .headers(headers)
        .contentType(MediaType.APPLICATION_OCTET_STREAM)
        .body(new InputStreamResource(converter.getTemplate()));
  }

  @ResponseStatus(HttpStatus.OK)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public Map<String, String> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
    Map<String, String> errors = new HashMap<>();

    ex.getBindingResult()
        .getFieldErrors()
        .forEach(
            error -> {
              errors.put(error.getField(), error.getDefaultMessage());
              System.out.println(
                  "error.getField()::"
                      + error.getField()
                      + " error.getDefaultMessage()::"
                      + error.getDefaultMessage());
            });

    return errors;
  }

  // GENERATED METHODS
  @PutMapping("/{id}")
  @PreAuthorize("hasAuthority('USER_WRITE_PRIVILEGE')")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<User> updateUser(@RequestBody User User, @PathVariable String id)
      throws Exception {
    Authentication authentication = authenticationFacade.getAuthentication();
    String userid = authentication.getName();
    AtomicReference<Boolean> isAdmin = new AtomicReference<>(false);
    authentication
        .getAuthorities()
        .forEach(
            s -> {
              //     if(s.getAuthority().toLowerCase().contains("admin")) isAdmin.set(true);
              if (userid.equals("admin")) isAdmin.set(true);
            });

    if (api.find(id, false, userid, isAdmin, false) == null) {
      throw new NotFoundException("id not found");
    }
    User.setId(id);
    api.update(User);

    return ResponseEntity.ok().body(User);
  }

  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  @PreAuthorize("hasAuthority('USER_WRITE_PRIVILEGE')")
  public ResponseEntity<User> createUser(@Valid @RequestBody User User) throws Exception {
    if (api.findBy("id", User.getId()) != null) {
      throw new ConflictException(
          "unique constraint violated, duplicate field: id -> " + User.getId());
    }
    Authentication authentication = authenticationFacade.getAuthentication();
    String userid = authentication.getName();

    User.setCarId(0);

    User.setCredit((double) 0);

    User.setPassword(encoder.encode(User.getPassword()));
    api.create(User);

    return ResponseEntity.created(URI.create(User.getId())).body(User);
  }

  @PostMapping(value = "/import")
  @PreAuthorize("hasAuthority('USER_WRITE_PRIVILEGE')")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<List<User>> importUsers(@RequestPart("file") MultipartFile filePart)
      throws Exception {

    List<User> Users =
        converter.csvToPojo(Collections.singletonList(new String(filePart.getBytes())));
    Authentication authentication = authenticationFacade.getAuthentication();
    String userid = authentication.getName();

    for (User User : Users) {

      User.setCarId(0);

      User.setCredit((double) 0);

      User.setPassword(encoder.encode(User.getPassword()));
      api.create(User);
    }
    return ResponseEntity.ok(Users);
  }

  @Value("classpath:json/USER.json")
  Resource menufile;

  @GetMapping(path = "getEntityFields")
  @ResponseStatus(HttpStatus.OK)
  @PreAuthorize("hasAuthority('USER_READ_PRIVILEGE')")
  public ResponseEntity<String> getEntityFields() throws Exception {
    InputStream EntityListContent = menufile.getInputStream();
    StringBuilder fileContent = new StringBuilder();
    BufferedReader bufferedReader = null;
    try {
      bufferedReader =
          //        new BufferedReader(new FileReader(new
          // File(String.valueOf(menufile.getFile()))));
          new BufferedReader(new InputStreamReader(EntityListContent));

      String line;
      while ((line = bufferedReader.readLine()) != null) {
        fileContent.append(line);
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (bufferedReader != null) {
        try {
          bufferedReader.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    String arrayToJson = fileContent.toString();
    return ResponseEntity.ok().body(arrayToJson);
  }

  @PatchMapping(path = "/{id}")
  public ResponseEntity<User> updateUser(
      @PathVariable String id, @RequestBody Map<String, Object> patch) {
    try {
      Authentication authentication = authenticationFacade.getAuthentication();
      String userid = authentication.getName();
      AtomicReference<Boolean> isAdmin = new AtomicReference<>(false);
      authentication
          .getAuthorities()
          .forEach(
              s -> {
                //  if (s.getAuthority().toLowerCase().contains("admin")) isAdmin.set(true);
                if (userid.equals("admin")) isAdmin.set(true);
              });

      User customer = api.find(id, false, userid, isAdmin, false);
      if (customer != null) {
        patch.forEach(
            (change, value) -> {
              switch (change) {
                case "id":
                  customer.setId((String) value);
                  break;
                case "Active":
                  customer.setActive((Boolean) value);
                  break;
                case "Birthday":
                  customer.setBirthday((String) value);
                  break;
                case "CarId":
                  customer.setCarId((Integer) value);
                  break;
                case "Email":
                  customer.setEmail((String) value);
                  break;
                case "ExpiredAte":
                  customer.setExpiredAte((Date) value);
                  break;
                case "FirstName":
                  customer.setFirstName((String) value);
                  break;
                case "Lang":
                  customer.setLang((String) value);
                  break;
                case "LastName":
                  customer.setLastName((String) value);
                  break;
                case "Provider":
                  customer.setProvider((AuthProvider) value);
                  break;
                case "ProviderId":
                  customer.setProviderId((String) value);
                  break;
                case "ImageUrl":
                  customer.setImageUrl((String) value);
                  break;
                case "MiddleName":
                  customer.setMiddleName((String) value);
                  break;
                case "MobilePhone":
                  customer.setMobilePhone((String) value);
                  break;
                case "Msn":
                  customer.setMsn((String) value);
                  break;
                case "Name":
                  customer.setName((String) value);
                  break;
                case "OfficePhone":
                  customer.setOfficePhone((String) value);
                  break;
                case "Otsmppconnectionherims":
                  customer.setOtsmppconnectionherims((String) value);
                  break;
                case "ParentManagerUserId":
                  customer.setParentManagerUserId((String) value);
                  break;
                case "Phone":
                  customer.setPhone((String) value);
                  break;
                case "Position":
                  customer.setPosition((String) value);
                  break;
                case "SendAlarm":
                  customer.setSendAlarm((Boolean) value);
                  break;
                case "SendInvoice":
                  customer.setSendInvoice((Boolean) value);
                  break;
                case "SendRateChange":
                  customer.setSendRateChange((Boolean) value);
                  break;
                case "SkyPe":
                  customer.setSkyPe((String) value);
                  break;
                case "TimeOffset":
                  customer.setTimeOffset((BigDecimal) value);
                  break;
                case "UserRoles":
                  customer.setUserRoles((ArrayList<String>) value);
                  break;
                case "ZipCode":
                  customer.setZipCode((String) value);
                  break;
                case "UserName":
                  customer.setUserName((String) value);
                  break;
                case "FriendlyName":
                  customer.setFriendlyName((String) value);
                  break;
                case "Address":
                  customer.setAddress((String) value);
                  break;
                case "Credit":
                  customer.setCredit((Double) value);
                  break;
                case "Roles":
                  customer.setRoles((ArrayList<String>) value);
                  break;
                case "Password":
                  customer.setPassword((String) value);
                  break;
              }
            });
        api.update(customer);
      }
      return ResponseEntity.ok(customer);
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }
}
