package dev.calligo.simpleapp.services.generated;

import dev.calligo.simpleapp.models.generated.*;
import dev.calligo.simpleapp.models.generated.Merchant;
import dev.calligo.simpleapp.enums.generated.*;
import dev.calligo.simpleapp.search.SearchCriteria;
import org.springframework.data.domain.Page;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public interface MerchantDao {

  /*
   create single Merchant
  */
  Merchant create(Merchant Merchant);

  /*
   create multiple Merchants
  */
  List<Merchant> create(List<Merchant> Merchants);

  /*
   update single Merchant
  */
  Merchant update(Merchant Merchant);

  /*
   delete single Merchant
  */
  void delete(String id);

  /*
   delete multiple Merchants
  */
  void deleteMany(List<String> ids);

  /*
   delete all Merchants
  */
  void deleteAll();

  /*
   find user by id
  */
  Merchant find(
      String Id, boolean cache, String userid, AtomicReference<Boolean> isAdmin, Boolean hasfield);

  /*
   search for Merchants using search criteria
  */
  Page<Merchant> search(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      int page,
      int size,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  long count(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      int size,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  /*
   search for Merchants using search criteria - non paginated
  */
  List<Merchant> search(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  /*
   find by field
  */
  Merchant findBy(String fieldName, String value);

  List<Merchant> getall(ArrayList<String> fieldNames);
}
