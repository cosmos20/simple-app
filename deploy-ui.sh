#!/usr/bin/env bash

export AWS_DEFAULT_OUTPUT="table"
export TASK_DEFINITION_REV="1"
export AWS_PAGER=""
export GITLAB_TOKEN=""
tg_arn=""
rule_arn=""
priority="-1"
definition="1"

function install_aws_cli() {
    echo "== installing aws cli on ${OSTYPE}"
    if [[ "${OSTYPE}" == "linux-gnu"* ]]; then
            curl -s "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
            unzip awscliv2.zip && \
            sudo ./aws/install --update
    elif [[ "${OSTYPE}" == "darwin"* ]]; then
            curl -s "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg" && \
            sudo installer -pkg AWSCLIV2.pkg -target /
    else
            echo "OS unknown"
            exit 1
    fi
}

function install_jq() {
    echo "== installing jq on ${OSTYPE}"
    if [[ "${OSTYPE}" == "linux-gnu"* ]]; then
            sudo apt install jq -y
    elif [[ "${OSTYPE}" == "darwin"* ]]; then
            brew install jq
    else
            echo "OS unknown"
            exit 1
    fi
}

function check_aws_cli() {
  echo "== checking if AWS CLI exist"
  if [[ "$(aws --version)" =~ aws-cli* ]]; then
    return 0
  else
    return 1
  fi
}

function check_jq() {
  echo "== checking if jq exist"
  which "jq"
  return $?
}

function cleanup() {
    echo "== cleaning up"
    rm -rf awscliv2.zip 2>/dev/null && \
    rm -rf aws 2>/dev/null && \
    rm -f temp.json 2>/dev/null
    rm -f AWSCLIV2.pkg 2>/dev/null
    rm -rf repo 2> /dev/null
}

function create_ecr() {
  echo "== creating cosmos20/simpleapp-ui registry in ECR"
  aws ecr describe-repositories --repository-names cosmos20/simpleapp-ui --no-paginate || aws ecr create-repository --repository-name cosmos20/simpleapp-ui --no-paginate
}

function get_aws_credentials(){
  echo "ENTER AWS_ACCESS_KEY_ID: "
  read -r -s ACCESS_KEY_ID
  export AWS_ACCESS_KEY_ID=${ACCESS_KEY_ID}

  echo "ENTER AWS_SECRET_ACCESS_KEY: "
  read -r -s SECRET_ACCESS_KEY
  export AWS_SECRET_ACCESS_KEY=${SECRET_ACCESS_KEY}

  echo "ENTER AWS REGION: "
  read -r REGION
  export AWS_DEFAULT_REGION=${REGION}

  mkdir ~/.aws 2>/dev/null

  cat <<EOF > ~/.aws/credentials
[default]
aws_access_key_id=${AWS_ACCESS_KEY_ID}
aws_secret_access_key=${AWS_SECRET_ACCESS_KEY}
EOF
  cat <<EOF > ~/.aws/config
[default]
region=${REGION}
EOF
}

function load_aws_credentials() {
  echo "== loading aws credentials"
  export AWS_ACCESS_KEY_ID=$(cat ~/.aws/credentials | grep aws_access_key_id | cut -d'=' -f2 | tr -d ' ')
  export AWS_SECRET_ACCESS_KEY=$(cat ~/.aws/credentials | grep aws_secret_access_key | cut -d'=' -f2 | tr -d ' ')
  export AWS_DEFAULT_REGION=$(cat ~/.aws/config | grep region | cut -d'=' -f2 | tr -d ' ')
}

function get_gitlab_credentials(){
  echo "ENTER GITLAB PRIVATE TOKEN: "
  read -r -s GTOKEN
  export GITLAB_TOKEN=${GTOKEN}
}

function check_cluster() {
    echo "== checking if cluster calligo exist"
    CLUSTER_LIST=$(aws ecs list-clusters --no-paginate)
    if [[ ${CLUSTER_LIST} =~ cluster/calligo ]];then
      return 0
    else
        echo ">>> cluster doesn't exist, please create it manually <<<"
        return 1
    fi
}

function create_task_definition() {
    echo "== creating task definition"
    aws ecs register-task-definition --region us-east-1 --family simpleapp-ui --output json --cli-input-json file://_deploy/ecs_deploy_ui.json > temp.json
    if [[ -f temp.json ]];then
        definition=`grep  '"revision"' temp.json | awk '{print $2}' | cut -d',' -f1`
    fi
}

function create_dns_record(){
  echo "== creating dns record"
  aws route53 change-resource-record-sets --hosted-zone-id Z0332507E0IGPCHVWVKU \
  --change-batch '{ "Comment": "record for simpleapp ui",
  "Changes": [ { "Action": "UPSERT", "ResourceRecordSet": { "Name":
  "simpleapp.calligo.dev", "Type": "A",
        "AliasTarget":{
            "HostedZoneId": "Z35SXDOTRQ7X7K",
            "DNSName": "dualstack.smshub-1447571642.us-east-1.elb.amazonaws.com",
            "EvaluateTargetHealth": false
                    }
            } }
            ]
    }' --no-paginate
}

function check_aws_credentials() {
  echo "== checking aws credentials"
  aws --region us-east-1 sts get-caller-identity  --endpoint-url https://sts.us-east-1.amazonaws.com --no-paginate
}

function create_target_group() {
  echo "== creating target group"
  aws elbv2 describe-target-groups --name simpleapp-ui || \
  aws elbv2 create-target-group \
    --name simpleapp-ui\
    --protocol HTTP \
    --port 80 \
    --target-type ip \
    --vpc-id vpc-03db3156835041491 \
    --health-check-path /health \
    --health-check-port 80 \
    --health-check-interval-seconds 20 \
    --health-check-timeout-seconds 5 \
    --healthy-threshold-count 2 \
    --unhealthy-threshold-count 5
  tg_arn=$(aws elbv2 describe-target-groups --names simpleapp-ui| grep 'TargetGroupArn' | cut -d '|' -f 4 | tr -d ' ')
}

function create_service() {
  echo "== creating service"
      aws ecs create-service \
        --cluster calligo  \
        --service-name simpleapp-ui \
        --task-definition simpleapp-ui:${definition} \
        --desired-count 1 \
        --launch-type FARGATE \
        --platform-version 1.3.0 \
        --load-balancers "targetGroupArn=${tg_arn},containerName=simpleapp-ui,containerPort=80" \
        --scheduling-strategy REPLICA \
        --network-configuration "awsvpcConfiguration={subnets=[subnet-0ec497cb992c2c7b6,subnet-08fadf345b1e924ce],securityGroups=[sg-0e34fa05003e5d40b],assignPublicIp=DISABLED}" \
      || \
      aws ecs update-service \
        --cluster calligo  \
        --service simpleapp-ui \
        --task-definition simpleapp-ui:${definition} \
        --desired-count 1 \
        --platform-version 1.3.0 \
        --network-configuration "awsvpcConfiguration={subnets=[subnet-0ec497cb992c2c7b6,subnet-08fadf345b1e924ce],securityGroups=[sg-0e34fa05003e5d40b],assignPublicIp=DISABLED}"
}

function create_elb_rule(){
  echo "== creating elb rule"
  current_rules=$(aws elbv2 describe-rules \
    --listener-arn arn:aws:elasticloadbalancing:us-east-1:487932330115:listener/app/smshub/9497af0ec2876ba3/4db8884fb46feff9 \
    --output json | \
    grep -w ${tg_arn})
  if [[ -z ${current_rules} ]];then
      rule_arn=$(aws elbv2 create-rule \
      --listener-arn arn:aws:elasticloadbalancing:us-east-1:487932330115:listener/app/smshub/9497af0ec2876ba3/4db8884fb46feff9 \
      --conditions "[{\"Field\": \"host-header\", \"Values\" : [\"simpleapp.calligo.dev\"]}]" \
      --actions "[{\"Type\": \"forward\", \"TargetGroupArn\": \"${tg_arn}\"}]" \
      --priority ${priority} \
      --tags "[{\"Key\": \"service_name\", \"Value\": \"simpleapp-ui\"}]" 2>&1)
      echo ${rule_arn} | perl -pe 's/.*(arn:aws.*listener.*?)\s+\|.*/$1/' > rule_arn_ui.tmp
  fi
}

function get_priority(){
  echo "== get priority"
  current_priority=$(aws elbv2 describe-rules \
    --listener-arn arn:aws:elasticloadbalancing:us-east-1:487932330115:listener/app/smshub/9497af0ec2876ba3/4db8884fb46feff9 \
    --output json | \
    grep Priority | \
    cut -d '"' -f4 | \
    sort -n | \
    tail -1
  )
  priority=$(echo ${current_priority} + 1 | bc)
}

function get_from_git(){
  if [[ -d repo/.git ]];then
    echo "ui repo exists"
  else
    git clone https://oauth2:${GTOKEN}@gitlab.com/cosmos20/angularui.git
    cd angularui
    git checkout develop
    cd ..
    mv angularui repo
  fi
}

function create_image(){
  cd repo
  git checkout develop
  sed -i '' "s/REPLACE_BASE_URL/https:\/\/api-simpleapp.calligo.dev/g" ./src/environments/environment.custom.ts
  npm install
  ng build --configuration=custom
  aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 487932330115.dkr.ecr.us-east-1.amazonaws.com
  if [[ $(uname -a) =~ ARM64 ]];then
    docker buildx build -f _docker/Dockerfile --platform linux/amd64 --push -t 487932330115.dkr.ecr.us-east-1.amazonaws.com/cosmos20/simpleapp-ui:latest  .
  else
    docker build -t 487932330115.dkr.ecr.us-east-1.amazonaws.com/cosmos20/simpleapp-ui:latest -f _docker/Dockerfile .
    docker push 487932330115.dkr.ecr.us-east-1.amazonaws.com/cosmos20/simpleapp-ui:latest
  fi
}

function check_docker(){
    echo "== checking if Docker is installed"
    docker info
}

function install_docker(){
    echo "== installing docker on ${OSTYPE}"
    if [[ "${OSTYPE}" == "linux-gnu"* ]]; then
            echo "not implemented"
            exit 1
    elif [[ "${OSTYPE}" == "darwin"* ]]; then
            echo "not implemented"
            exit 1
    else
            echo "OS unknown"
            exit 1
    fi
}

function check_build_tools() {
    echo "== checking if angular build tools are installed"
    which npm || \
    return 1 && \
    which ng || \
    return 1
}

function install_build_tools(){
    echo "== installing docker on ${OSTYPE}"
    if [[ "${OSTYPE}" == "linux-gnu"* ]]; then
            echo "not implemented"
            exit 1
    elif [[ "${OSTYPE}" == "darwin"* ]]; then
            echo "not implemented"
            exit 1
    else
            echo "OS unknown"
            exit 1
    fi
}

ROOT_FOLDER=$(pwd)
# check npm & ng build
check_build_tools ||
# install npm and ng-cli
install_build_tools && \
# check docker
check_docker || \
# install docker
install_docker && \
# get gitlab credentials
get_gitlab_credentials && \
# get from git
get_from_git && \
# check credentials
check_aws_credentials || \
# get aws credentials
get_aws_credentials && \
# load aws credentials
load_aws_credentials &&
# check aws cli
check_aws_cli || \
# install aws cli
install_aws_cli && \
# check jq
check_jq || \
# install aws cli
install_jq && \
# create registry (ecr)
create_ecr && \
# create image
create_image && \
# got to root folder
cd ${ROOT_FOLDER} && \
# check cluster (ecs)
check_cluster && \
# create task definition (ecs)
create_task_definition && \
# create dns record to resource(alb)
create_dns_record && \
# create target group
create_target_group && \
# get priority
get_priority && \
# create elb rule
create_elb_rule && \
# create service
create_service
cleanup

