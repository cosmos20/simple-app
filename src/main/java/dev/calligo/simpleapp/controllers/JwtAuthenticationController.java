package dev.calligo.simpleapp.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import dev.calligo.simpleapp.conf.JwtTokenUtil;
import dev.calligo.simpleapp.enums.generated.AuthProvider;
import dev.calligo.simpleapp.exceptions.BadRequestException;
import dev.calligo.simpleapp.exceptions.OAuth2AuthenticationProcessingException;
import dev.calligo.simpleapp.exceptions.global.ForbiddenException;
import dev.calligo.simpleapp.exceptions.global.UnauthorizedException;
import dev.calligo.simpleapp.models.generated.Privilege;
import dev.calligo.simpleapp.models.generated.Role;
import dev.calligo.simpleapp.models.generated.SignUpRequest;
import dev.calligo.simpleapp.models.generated.SignUpResponse;
import dev.calligo.simpleapp.models.generated.User;
import dev.calligo.simpleapp.security.*;
import dev.calligo.simpleapp.security.oauth2.user.OAuth2UserInfo;
import dev.calligo.simpleapp.security.oauth2.user.OAuth2UserInfoFactory;
import dev.calligo.simpleapp.services.generated.PrivilegeDao;
import dev.calligo.simpleapp.services.generated.UserDao;
import dev.calligo.simpleapp.services.generated.RoleDao;
import dev.calligo.simpleapp.utils.Tools;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import com.google.firebase.auth.UserRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import dev.calligo.simpleapp.service.FirebaseInitializer;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import javax.validation.Valid;
import java.net.URI;
import java.net.URLEncoder;
import java.util.*;
import java.io.FileInputStream;
import java.util.concurrent.atomic.AtomicReference;
import java.io.FileInputStream;
import java.io.InputStream;


@RestController
@RequestMapping("/")
public class JwtAuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userRepository;
    //private CustomUserDetailsService userRepository;
    @Autowired
    private  FirebaseInitializer firebaseInitializer;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Autowired private JwtTokenUtil jwtTokenUtil;

    @Autowired PasswordEncoder encoder;

    @Autowired
    UserDao UserDao;
    @Autowired
    PrivilegeDao privilegeDao;
    @Autowired
    Tools tools;

    @Autowired
    RoleDao roleDao;



    @Autowired
    private TokenProvider tokenProvider;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<JwtResponse> authenticateUser(@Valid @RequestBody JwtRequest loginRequest) {
        System.out.println("loginRequest = " + loginRequest);
//    Authentication authentication = authenticationManager.authenticate(
//            new UsernamePasswordAuthenticationToken(
//                    loginRequest.getUserName(),
//                    loginRequest.getPassword()
//            )
//    );
//
//    SecurityContextHolder.getContext().setAuthentication(authentication);
//
//    String token = tokenProvider.createToken(authentication);
//    return ResponseEntity.ok(new JwtResponse(token));


        try {
            authenticate(loginRequest.getUsername(), loginRequest.getPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }

        final UserDetails userDetails =
                userRepository.loadUserByUsername(loginRequest.getUsername());

        String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponse(token));

    }
    private void authenticate(String username, String password) throws Exception {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new ForbiddenException("User disabled");
        } catch (BadCredentialsException e) {
            throw new UnauthorizedException("Invalid Credentials");
        }
    }


                                                                                                                                                                                                                                    


    @PostMapping("/signup/google")
    public ResponseEntity<SignUpResponse> registerUsergoogle(@Valid @RequestBody SignUpRequest signUpRequest) {
        User user = new User();
        try{
            UserRecord user_id = check_GooGle_tokenValidity(signUpRequest.getFbtoken());
            String email = user_id.getEmail();
            System.out.println("user_id.getEmail() = " + user_id.getEmail());
            System.out.println("user_id.getProviderData()[0].getEmail() = " + user_id.getProviderData()[0].getEmail());
            if(email==null || email.equals("null")) email = user_id.getProviderData()[0].getEmail();
            if (user_id!=null) {
                Optional<User> userOptional = Optional.ofNullable(UserDao.findBy("email", email));
//         OAuth2UserInfo oAuth2UserInfo = OAuth2UserInfoFactory.getOAuth2UserInfo(AuthProvider.GooGle.toString(), user_id);

                if (userOptional.isPresent()) {
                    user = userOptional.get();

                    user = updateExistingUser(AuthProvider.GooGle.toString(),user, user_id, "", email);
                    //authenticate(user.getUserName(), user.getPassword());
                    final UserDetails userDetails = UserPrincipal.create(user);
                    String token = jwtTokenUtil.generateToken(userDetails);
                    return ResponseEntity.ok(new SignUpResponse(token,user.getUserName()));
                } else {
                    user = registerNewUser(AuthProvider.GooGle.toString(), user_id, "", email);
                    //authenticate(user.getUserName(), user.getPassword());
                    final UserDetails userDetails = UserPrincipal.create(user);
                    String token = jwtTokenUtil.generateToken(userDetails);
                    return ResponseEntity.ok(new SignUpResponse(token,user.getUserName()));
                }
            }else{
                throw new OAuth2AuthenticationProcessingException("Could not validate ....");
            }


        } catch(Exception e){
            e.printStackTrace();
        }

        return ResponseEntity.ok(new SignUpResponse("FBToken is not valid","null"));

    }



    String check_FB_tokenValidity(String fbtoken){
        try{
            URI uri = URI.create("https://graph.facebook.com/debug_token?input_token=" +
//              URLEncoder.encode("EAACAjCXMe3QBANqVJsC1ZBAZAIIUeeUb7GAJXaJQOpgpZBnxHhZBHsZBCMbGhInpExBlXijdj9lH3xvSNij3qTdQnzqwX8JEJfb0XI9SG8AljzkxDWsLmts50FbJ5P4FgQuZAdF7LziOdB4K3ZBnUjPKiuOU8pB8M9PTGHmiL3nOpUbyvvxxeV3") +
                    URLEncoder.encode(fbtoken) +
                    "&access_token=" +
                    URLEncoder.encode( "141339417934708|Pfq836GaKnolqnecpmYpIHM80nE"));
            System.out.println("uri.toString() = " + uri.toString());
            RestTemplate restTemplate = new RestTemplate();

            JsonNode resp = null;
            try {
                resp = restTemplate.getForObject(uri, JsonNode.class);
            } catch (HttpClientErrorException e) {
                e.printStackTrace();
            }
            System.out.println("resp = " + resp);

            Gson gson = new Gson();
            Map jsonElement = gson.fromJson(String.valueOf(resp), Map.class);

//      System.out.println("jsonElement.get(\"url\") = " + ((Map)jsonElement.get("data")).get("user_id"));

            return String.valueOf(((Map)jsonElement.get("data")).get("user_id"));
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }
    @Autowired
    private  ResourceLoader resourceLoader;

    UserRecord check_GooGle_tokenValidity(String fbtoken){
        try{
            String filePath = "classpath:/firebase/key.json";
            Resource resource= resourceLoader.getResource(filePath);
            InputStream refreshToken= resource.getInputStream();

            FirebaseOptions options = FirebaseOptions.builder()
                    .setCredentials(GoogleCredentials.fromStream(refreshToken))
                    //      .setDatabaseUrl("firebase-adminsdk-qc97z@ko-fitness-90a5b.iam.gserviceaccount.com")
                    .build();
            if(FirebaseApp.getApps().isEmpty()){
                FirebaseApp.initializeApp(options);
            }

            FirebaseToken decodedToken = FirebaseAuth.getInstance().verifyIdToken(fbtoken);
            String uid = decodedToken.getUid();
            UserRecord u = FirebaseAuth.getInstance().getUser(uid);
            return u;

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
    private User registerNewUser(String oAuth2UserRequest, UserRecord oAuth2UserInfo, String password, String email) {
        User user = new User();

        user.setId(String.valueOf(UUID.randomUUID()));
        //user.setProvider(AuthProvider.valueOf(oAuth2UserRequest));
        user.setProviderId(oAuth2UserInfo.getProviderId());
        user.setName(oAuth2UserInfo.getDisplayName());
        user.setUserName(email);
        user.setEmail(email);
        user.setImageUrl(oAuth2UserInfo.getPhotoUrl());

//    List<GrantedAuthority> authorities = new ArrayList<>();
        ArrayList<String> roles = new ArrayList<>();
        List<Privilege> privileges = new ArrayList<>();

        try {
            Role privilege = roleDao.findBy("friendlyName", "USER_ROLE");
//        authorities.add(new SimpleGrantedAuthority(privilege.getFriendlyName()));
            System.out.println("privilege.getId() = " + privilege.getId());
            roles.add(privilege.getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        user.setRoles(roles);
        user.setPassword(encoder.encode(password));


        return UserDao.create(user);
    }


    private User updateExistingUser(String oAuth2UserRequest, User user, UserRecord oAuth2UserInfo, String password, String email) {
        user.setProvider(AuthProvider.valueOf(oAuth2UserRequest));
        user.setProviderId(oAuth2UserInfo.getProviderId());
        user.setName(oAuth2UserInfo.getDisplayName());
        user.setUserName(email);
        user.setEmail(email);
        user.setImageUrl(oAuth2UserInfo.getPhotoUrl());

//    List<GrantedAuthority> authorities = new ArrayList<>();
        List<Privilege> privileges = new ArrayList<>();

        ArrayList<String> roles = new ArrayList<>();
        try {
            for(String ss:user.getRoles()) {
                System.out.println("ss = " + ss);
                Role r = roleDao.findBy("id", ss);
                System.out.println("r.getId() = " + r.getId());
                roles.add(r.getId());

//                if (r != null) {
//                    r.getPrivileges()
//                            .forEach(
//                                    s -> {
//                                        System.out.println("s = " + s);
//                                        privileges.add(s);
//                                    });
//                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        user.setRoles(roles);
        user.setPassword(encoder.encode(password));
        return UserDao.update(user);
    }

}
