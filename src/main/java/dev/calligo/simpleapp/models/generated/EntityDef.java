package dev.calligo.simpleapp.models.generated;

import com.github.javafaker.Faker;
import io.swagger.annotations.ApiModel;
import dev.calligo.simpleapp.enums.generated.*;
import lombok.Setter;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.*;
import org.springframework.http.MediaType;
import javax.validation.constraints.*;
import org.springframework.data.mongodb.core.mapping.Field;
import com.google.gson.annotations.SerializedName;

@ApiModel(description = "EntityDef model")
public class EntityDef implements Serializable {
  Map<String, ArrayList<String>> arrayofarray;

  @Getter
  @Setter
  @Field("_id")
  @Id
  @SerializedName("id")
  private String id;

  @Getter
  @Setter
  @SerializedName("FriendlyName")
  private String FriendlyName;

  @Getter
  @Setter
  @SerializedName("Name")
  private String Name;

  @Getter
  @Setter
  @SerializedName("EndPoint")
  private String EndPoint;

  @Getter
  @Setter
  @SerializedName("Fields")
  private ArrayList<FieldDef> Fields;

  @Getter
  @Setter
  @SerializedName("Setting")
  private ArrayList<EntityConf> Setting;

  public EntityDef generateFake() {
    Faker faker = new Faker();
    EntityDef ret = new EntityDef();
    ret.setId(String.valueOf(UUID.randomUUID()));

    ret.setFriendlyName(faker.name().name());

    ret.setName(faker.name().name());

    ret.setEndPoint(faker.name().name());

    return ret;
  }

  public String toCSV(String attach) {
    String ret = attach;
    ret =
        ret
            + (ret.length() > 3 ? "," : "")
            + this.id
            + ","
            + this.FriendlyName
            + ","
            + this.Name
            + ","
            + this.EndPoint
            + ","
            + toCSV_FieldDef(this.Fields)
            + ","
            + toCSV_EntityConf(this.Setting);
    return ret;
  }

  private String toCSV_FieldDef(ArrayList<FieldDef> fields) {
    String ret = "";
    ArrayList<String> rr = new ArrayList<>();
    for (FieldDef df : Fields) rr.add(df.toCSV(""));
    return String.join(",", rr);
  }

  private String toCSV_EntityConf(ArrayList<EntityConf> fields) {
    String ret = "";
    ArrayList<String> rr = new ArrayList<>();
    for (EntityConf df : Setting) rr.add(df.toCSV(""));
    return String.join(",", rr);
  }

  private String leverage(String line, Map<String, ArrayList<String>> arrayofarray) {
    String ret = "";
    Map<String, ArrayList<String>> arrayofarray2 = arrayofarray;
    ArrayList<String> arrayofarray3 = new ArrayList<>();
    arrayofarray3.addAll(arrayofarray.keySet());
    for (String s : arrayofarray3) {
      if (arrayofarray != null && arrayofarray.size() > 0) {
        for (String d : arrayofarray.get(s.toUpperCase())) {
          String ll = line.replaceAll(s.toUpperCase(), d);
          Map<String, ArrayList<String>> arrayofarray22 = arrayofarray2;
          arrayofarray22.remove(s);
          if (arrayofarray22.size() > 0) ret = ret + "" + leverage(ll, arrayofarray22);
          else ret = ret + "" + ll;
        }
      }
    }

    return ret;
  }

  private String toleverage(ArrayList<String> clientproducts, String replacement) {
    if (arrayofarray == null) arrayofarray = new HashMap<>();
    if (clientproducts != null && clientproducts.size() > 0) {
      ArrayList<String> datas = arrayofarray.get(replacement);
      if (datas == null) datas = new ArrayList<>();
      for (String s : clientproducts) datas.add(s);
      arrayofarray.put(replacement, datas);
    } else replacement = "";
    return replacement;
  }

  @Override
  public String toString() {
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      return objectMapper.writeValueAsString(this);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
      return null;
    }
  }
}
