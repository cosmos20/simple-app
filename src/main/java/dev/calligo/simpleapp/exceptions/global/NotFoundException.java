package dev.calligo.simpleapp.exceptions.global;

public class NotFoundException extends RuntimeException {
  public NotFoundException(String msg) {
    super(msg);
  }
}
