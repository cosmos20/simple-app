package dev.calligo.simpleapp.utils;

import dev.calligo.simpleapp.exceptions.global.InternalException;
import dev.calligo.simpleapp.exceptions.global.InvalidRequestException;
import dev.calligo.simpleapp.models.generated.EntityDef;
import dev.calligo.simpleapp.models.generated.FieldDef;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class Exporter {

    @Autowired public EntitiesUtil entitiesUtil;

    @Autowired
    public Exporter() {}

    public ByteArrayInputStream export(List<Object> objects, String fields, String type)
            throws Exception {
        if (objects.size() == 0) {
            return new ByteArrayInputStream(new byte[0]);
        }
        Object refObj = objects.get(0);
        String entityName = refObj.getClass().getSimpleName();
        EntityDef en = entitiesUtil.entityMap.get(entityName);
        if (en == null) {
            throw new InternalException("entity not found");
        }
        List<String> fs = new ArrayList<>();

        if (fields != null) {
            fs = Arrays.asList(fields.split(","));
        }

        switch (type.toLowerCase()) {
            case "pdf":
                return getPDF(objects, fs, en);
            case "excel":
                return getExcel(objects, fs, en);
            default:
                throw new InvalidRequestException("invalid type");
        }
    }

    public ByteArrayInputStream getPDF(List<Object> objects, List<String> fs, EntityDef en)
            throws Exception {
        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        PdfPCell hcell;
        PdfPTable table;
        if (fs.size() == 0) {
            table = new PdfPTable(en.getFields().size());
            for (FieldDef f : en.getFields()) {
                hcell = new PdfPCell(new Phrase(f.getFieldName(), headFont));
                hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(hcell);
            }
        } else {
            table = new PdfPTable(fs.size());
            for (String s : fs) {
                hcell = new PdfPCell(new Phrase(s, headFont));
                hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(hcell);
            }
        }

        table.setWidthPercentage(100);
        for (Object obj : objects) {
            PdfPCell cell;
            if (fs.size() == 0) {
                for (FieldDef f : en.getFields()) {
                    Object value = PropertyUtils.getProperty(obj, f.getFieldName());
                    Phrase pr = Phrase.getInstance(String.valueOf(value));
                    cell = new PdfPCell(pr);
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);
                }
            } else {
                for (String s : fs) {
                    Object value = PropertyUtils.getProperty(obj, s);
                    Phrase pr = Phrase.getInstance(String.valueOf(value));
                    cell = new PdfPCell(pr);
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);
                }
            }
        }

        PdfWriter.getInstance(document, out);
        document.setPageSize(PageSize.A4.rotate());
        document.open();
        document.add(table);
        document.close();
        return new ByteArrayInputStream(out.toByteArray());
    }

    public ByteArrayInputStream getExcel(List<Object> objects, List<String> fs, EntityDef en)
            throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(en.getName());
        int headerColNum = 0;
        Row headerRow = sheet.createRow(0);
        if (fs.size() == 0) {
            for (FieldDef f : en.getFields()) {
                Cell headerCell = headerRow.createCell(headerColNum++);
                headerCell.setCellValue(f.getFieldName());
            }
        } else {
            for (String s : fs) {
                Cell headerCell = headerRow.createCell(headerColNum++);
                headerCell.setCellValue(s);
            }
        }

        int rowNum = 1;
        for (Object obj : objects) {
            int colNum = 0;
            Row row = sheet.createRow(rowNum);
            if (fs.size() == 0) {
                for (FieldDef f : en.getFields()) {
                    Cell cell = row.createCell(colNum++);
                    Object value = PropertyUtils.getProperty(obj, f.getFieldName());
                    cell.setCellValue(String.valueOf(value));
                }
            } else {
                for (String s : fs) {
                    Cell cell = row.createCell(colNum++);
                    Object value = PropertyUtils.getProperty(obj, s);
                    cell.setCellValue(String.valueOf(value));
                }
            }
            rowNum++;
        }
        workbook.write(out);
        return new ByteArrayInputStream(out.toByteArray());
    }
}
