package dev.calligo.simpleapp.search;

import lombok.Data;
import lombok.Setter;

@Data
@Setter
public class SearchCriteria {
  private String key;
  private String operation;
  private Object value;

  public SearchCriteria() {}

  public SearchCriteria(final String key, final String operation, final Object value) {
    super();
    this.key = key;
    this.operation = operation;
    this.value = value;
  }
}
