package dev.calligo.simpleapp.models.generated;

import com.github.javafaker.Faker;
import io.swagger.annotations.ApiModel;
import dev.calligo.simpleapp.enums.generated.*;
import lombok.Setter;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.*;
import org.springframework.http.MediaType;
import javax.validation.constraints.*;
import org.springframework.data.mongodb.core.mapping.Field;
import com.google.gson.annotations.SerializedName;

@ApiModel(description = "User model")
public class User implements Serializable {
  Map<String, ArrayList<String>> arrayofarray;

  @Getter
  @Setter
  @Field("_id")
  @Id
  @SerializedName("id")
  private String id;

  @Getter
  @Setter
  @SerializedName("Active")
  private Boolean Active;

  @Getter
  @Setter
  @SerializedName("Birthday")
  private String Birthday;

  @Getter
  @Setter
  @SerializedName("CarId")
  private Integer CarId;

  @Getter
  @Setter
  @SerializedName("Email")
  private String Email;

  @Getter
  @Setter
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @SerializedName("ExpiredAte")
  private Date ExpiredAte;

  @Getter
  @Setter
  @SerializedName("FirstName")
  private String FirstName;

  @Getter
  @Setter
  @SerializedName("Lang")
  private String Lang;

  @Getter
  @Setter
  @SerializedName("LastName")
  private String LastName;

  @Getter
  @Setter
  @SerializedName("Provider")
  private AuthProvider Provider;

  @Getter
  @Setter
  @SerializedName("ProviderId")
  private String ProviderId;

  @Getter
  @Setter
  @SerializedName("ImageUrl")
  private String ImageUrl;

  @Getter
  @Setter
  @SerializedName("MiddleName")
  private String MiddleName;

  @Getter
  @Setter
  @SerializedName("MobilePhone")
  private String MobilePhone;

  @Getter
  @Setter
  @SerializedName("Msn")
  private String Msn;

  @Getter
  @Setter
  @SerializedName("Name")
  private String Name;

  @Getter
  @Setter
  @SerializedName("OfficePhone")
  private String OfficePhone;

  @Getter
  @Setter
  @SerializedName("Otsmppconnectionherims")
  private String Otsmppconnectionherims;

  @Getter @Setter private String ParentManagerUserId;

  @Getter
  @Setter
  @SerializedName("Phone")
  private String Phone;

  @Getter
  @Setter
  @SerializedName("Position")
  private String Position;

  @Getter
  @Setter
  @SerializedName("SendAlarm")
  private Boolean SendAlarm;

  @Getter
  @Setter
  @SerializedName("SendInvoice")
  private Boolean SendInvoice;

  @Getter
  @Setter
  @SerializedName("SendRateChange")
  private Boolean SendRateChange;

  @Getter
  @Setter
  @SerializedName("SkyPe")
  private String SkyPe;

  @Getter
  @Setter
  @SerializedName("TimeOffset")
  private BigDecimal TimeOffset;

  @Getter @Setter private ArrayList<String> UserRoles;

  @Getter
  @Setter
  @SerializedName("ZipCode")
  private String ZipCode;

  @Getter
  @Setter
  @SerializedName("UserName")
  private String UserName;

  @Getter
  @Setter
  @SerializedName("FriendlyName")
  private String FriendlyName;

  @Getter
  @Setter
  @SerializedName("Address")
  private String Address;

  @Getter
  @Setter
  @SerializedName("Credit")
  private Double Credit;

  @Getter @Setter private ArrayList<String> Roles;

  @Getter
  @Setter
  @SerializedName("Password")
  private String Password;

  public User generateFake() {
    Faker faker = new Faker();
    User ret = new User();
    ret.setId(String.valueOf(UUID.randomUUID()));

    ret.setActive(true);

    ret.setBirthday(faker.name().name());

    ret.setEmail(faker.bothify("????##@gmail.com"));

    ret.setExpiredAte(new Date());

    ret.setFirstName(faker.name().name());

    ret.setLang(faker.name().name());

    ret.setLastName(faker.name().name());

    ret.setProviderId(faker.name().name());

    ret.setImageUrl(faker.name().name());

    ret.setMiddleName(faker.name().name());

    ret.setMobilePhone(faker.name().name());

    ret.setMsn(faker.name().name());

    ret.setName(faker.name().name());

    ret.setOfficePhone(faker.name().name());

    ret.setOtsmppconnectionherims(faker.name().name());

    ret.setPhone(faker.name().name());

    ret.setPosition(faker.name().name());

    ret.setSendAlarm(true);

    ret.setSendInvoice(true);

    ret.setSendRateChange(true);

    ret.setSkyPe(faker.name().name());

    ret.setZipCode(faker.name().name());

    ret.setUserName(faker.name().name());

    ret.setFriendlyName(faker.name().name());

    ret.setAddress(faker.name().name());

    ret.setCredit((double) faker.number().randomDigit());

    ret.setPassword(faker.name().name());
    return ret;
  }

  public String toCSV(String attach) {
    String ret = attach;
    ret =
        ret
            + (ret.length() > 3 ? "," : "")
            + this.id
            + ","
            + this.Active
            + ","
            + this.Birthday
            + ","
            + this.CarId
            + ","
            + this.Email
            + ","
            + this.ExpiredAte
            + ","
            + this.FirstName
            + ","
            + this.Lang
            + ","
            + this.LastName
            + ","
            + this.Provider
            + ","
            + this.ProviderId
            + ","
            + this.ImageUrl
            + ","
            + this.MiddleName
            + ","
            + this.MobilePhone
            + ","
            + this.Msn
            + ","
            + this.Name
            + ","
            + this.OfficePhone
            + ","
            + this.Otsmppconnectionherims
            + ","
            + this.ParentManagerUserId
            + ","
            + this.Phone
            + ","
            + this.Position
            + ","
            + this.SendAlarm
            + ","
            + this.SendInvoice
            + ","
            + this.SendRateChange
            + ","
            + this.SkyPe
            + ","
            + this.TimeOffset
            + ","
            + toleverage(this.UserRoles, "USERROLES")
            + ","
            + this.ZipCode
            + ","
            + this.UserName
            + ","
            + this.FriendlyName
            + ","
            + this.Address
            + ","
            + this.Credit
            + ","
            + toleverage(this.Roles, "ROLES")
            + ","
            + this.Password;
    ret = leverage(ret, arrayofarray);
    return ret;
  }

  private String leverage(String line, Map<String, ArrayList<String>> arrayofarray) {
    String ret = "";
    Map<String, ArrayList<String>> arrayofarray2 = arrayofarray;
    ArrayList<String> arrayofarray3 = new ArrayList<>();
    arrayofarray3.addAll(arrayofarray.keySet());
    for (String s : arrayofarray3) {
      if (arrayofarray != null && arrayofarray.size() > 0) {
        for (String d : arrayofarray.get(s.toUpperCase())) {
          String ll = line.replaceAll(s.toUpperCase(), d);
          Map<String, ArrayList<String>> arrayofarray22 = arrayofarray2;
          arrayofarray22.remove(s);
          if (arrayofarray22.size() > 0) ret = ret + "" + leverage(ll, arrayofarray22);
          else ret = ret + "" + ll;
        }
      }
    }

    return ret;
  }

  private String toleverage(ArrayList<String> clientproducts, String replacement) {
    if (arrayofarray == null) arrayofarray = new HashMap<>();
    if (clientproducts != null && clientproducts.size() > 0) {
      ArrayList<String> datas = arrayofarray.get(replacement);
      if (datas == null) datas = new ArrayList<>();
      for (String s : clientproducts) datas.add(s);
      arrayofarray.put(replacement, datas);
    } else replacement = "";
    return replacement;
  }

  @Override
  public String toString() {
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      return objectMapper.writeValueAsString(this);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
      return null;
    }
  }
}
