package dev.calligo.simpleapp.services.generated;

import dev.calligo.simpleapp.models.generated.*;
import dev.calligo.simpleapp.models.generated.EntityDef;
import dev.calligo.simpleapp.enums.generated.*;
import dev.calligo.simpleapp.search.SearchCriteria;
import org.springframework.data.domain.Page;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public interface EntityDefDao {

  /*
   create single EntityDef
  */
  EntityDef create(EntityDef EntityDef);

  /*
   create multiple EntityDefs
  */
  List<EntityDef> create(List<EntityDef> EntityDefs);

  /*
   update single EntityDef
  */
  EntityDef update(EntityDef EntityDef);

  /*
   delete single EntityDef
  */
  void delete(String id);

  /*
   delete multiple EntityDefs
  */
  void deleteMany(List<String> ids);

  /*
   delete all EntityDefs
  */
  void deleteAll();

  /*
   find user by id
  */
  EntityDef find(
      String Id, boolean cache, String userid, AtomicReference<Boolean> isAdmin, Boolean hasfield);

  /*
   search for EntityDefs using search criteria
  */
  Page<EntityDef> search(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      int page,
      int size,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  long count(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      int size,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  /*
   search for EntityDefs using search criteria - non paginated
  */
  List<EntityDef> search(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  /*
   find by field
  */
  EntityDef findBy(String fieldName, String value);

  List<EntityDef> getall(ArrayList<String> fieldNames);
}
