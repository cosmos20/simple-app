package dev.calligo.simpleapp.controllers.generated;

import dev.calligo.simpleapp.exceptions.global.ConflictException;
import dev.calligo.simpleapp.exceptions.global.InternalException;
import dev.calligo.simpleapp.exceptions.global.InvalidRequestException;
import dev.calligo.simpleapp.exceptions.global.NotFoundException;
import dev.calligo.simpleapp.utils.EntitiesUtil;
import dev.calligo.simpleapp.enums.generated.*;
import dev.calligo.simpleapp.converters.generated.*;
import dev.calligo.simpleapp.models.generated.Menu;
import dev.calligo.simpleapp.search.SearchCriteria;
import dev.calligo.simpleapp.security.AuthenticationFacade;
import dev.calligo.simpleapp.services.generated.*;
import dev.calligo.simpleapp.utils.Exporter;
import dev.calligo.simpleapp.utils.Tools;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.beans.factory.annotation.Value;
import java.io.*;
import org.springframework.data.domain.Page;
import javax.validation.Valid;
import org.springframework.web.bind.MethodArgumentNotValidException;
import dev.calligo.simpleapp.models.generated.*;
import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicReference;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

// GENERATED IMPORTS

@RestController
@RequestMapping(value = "/Menu", produces = "application/json")
public class MenuController {

  @Autowired MenuDao api;
  @Autowired AuthenticationFacade authenticationFacade;
  @Autowired PasswordEncoder encoder;
  @Autowired EntitiesUtil entitiesUtil;
  @Autowired Exporter exporter;
  @Autowired Tools tools;
  @Autowired MenuConverter converter;

  @RequestMapping(method = RequestMethod.GET)
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  @PreAuthorize("hasAuthority('MENU_READ_PRIVILEGE')")
  public ResponseEntity<?> getMenus(
      @RequestParam(value = "orderBy", required = false, defaultValue = "id") String orderBy,
      @RequestParam(value = "direction", required = false, defaultValue = "ASC") String direction,
      @RequestParam(value = "page", required = false, defaultValue = "0") int page,
      @RequestParam(value = "size", required = false, defaultValue = "20") int size,
      @RequestParam(value = "paginate", required = false, defaultValue = "true") boolean paginate,
      @RequestParam(value = "search", required = false) String search)
      throws Exception {
    List<SearchCriteria> params = tools.getSearchCriteria(search);
    if (!paginate) {
      return ResponseEntity.ok()
          .body(
              Myfilter(
                  api.search(
                      params, orderBy, direction, "", new AtomicReference<Boolean>(true), false),
                  "Menu"));
    }
    return ResponseEntity.ok()
        .body(
            Myfilter(
                api.search(
                    params,
                    orderBy,
                    direction,
                    page,
                    size,
                    "",
                    new AtomicReference<Boolean>(true),
                    false),
                "Menu"));
  }

  private Object Myfilter(Page<Menu> search, String Menu) {
    if (search == null) return null;
    else if (Menu.equals("Menu")) return search;
    else {
      Authentication authentication = authenticationFacade.getAuthentication();
      Set<String> carNames =
          authentication.getAuthorities().stream()
              .map(s -> s.toString().toString().replaceAll("_READ_PRIVILEGE", ""))
              .map(s -> s.toString().toString().replaceAll("_WRITE_PRIVILEGE", ""))
              .collect(Collectors.toSet());
      carNames.add("ALPHA");
      System.out.println("carNames = " + carNames);
      for (Menu m : search) {
        if (m.getFriendlyName() != null && carNames.contains(m.getFriendlyName().toUpperCase())) {

        } else {
          System.out.println(m.getFriendlyName());
        }
      }
      List<Menu> listOutput =
          search.stream()
              .filter(
                  car ->
                      car.getFriendlyName() != null
                          ? carNames.contains(
                              car.getFriendlyName().toUpperCase().replaceAll("/", ""))
                          : carNames.contains(null))
              .collect(Collectors.toList());
      System.out.println("listOutput = " + listOutput.size());

      return listOutput;
    }
  }

  private Object Myfilter(List<Menu> search, String Menu) {
    if (search == null) return null;
    else if (Menu.equals("Menu")) return search;
    else {
      Authentication authentication = authenticationFacade.getAuthentication();
      Set<String> carNames =
          authentication.getAuthorities().stream()
              .map(s -> s.toString().toString().replaceAll("_READ_PRIVILEGE", ""))
              .map(s -> s.toString().toString().replaceAll("_WRITE_PRIVILEGE", ""))
              .collect(Collectors.toSet());
      carNames.add("ALPHA");
      System.out.println("carNames = " + carNames);
      for (Menu m : search) {
        if (m.getFriendlyName() != null && carNames.contains(m.getFriendlyName().toUpperCase())) {

        } else {
          System.out.println(m.getFriendlyName());
        }
      }
      List<Menu> listOutput =
          search.stream()
              .filter(
                  car ->
                      car.getFriendlyName() != null
                          ? carNames.contains(
                              car.getFriendlyName().toUpperCase().replaceAll("/", ""))
                          : carNames.contains(null))
              .collect(Collectors.toList());
      System.out.println("listOutput = " + listOutput.size());

      return listOutput;
    }
  }

  @GetMapping(path = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @PreAuthorize("hasAuthority('MENU_READ_PRIVILEGE')")
  public ResponseEntity<Menu> getMenu(@PathVariable String id) {
    Menu item = api.find(id, true, "", new AtomicReference<Boolean>(true), false);
    if (item == null) {
      throw new NotFoundException("id not found");
    }
    return ResponseEntity.ok().body(item);
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PreAuthorize("hasAuthority('MENU_WRITE_PRIVILEGE')")
  public ResponseEntity<?> deleteMenu(@PathVariable String id) {
    if (api.find(id, false, "", new AtomicReference<Boolean>(true), false) == null) {
      throw new NotFoundException("id not found");
    }

    api.delete(id);

    return ResponseEntity.noContent().build();
  }

  @DeleteMapping("/ids")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  @PreAuthorize("hasAuthority('MENU_WRITE_PRIVILEGE')")
  public ResponseEntity<?> deleteMenus(@RequestParam List<String> ids) {
    api.deleteMany(ids);
    return ResponseEntity.noContent().build();
  }

  @RequestMapping(
      value = "export",
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_PDF_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE})
  @ResponseStatus(HttpStatus.OK)
  @PreAuthorize("hasAuthority('MENU_READ_PRIVILEGE')")
  public ResponseEntity<InputStreamResource> exportMenus(
      @RequestParam(value = "search", required = false) String search,
      @RequestParam(value = "orderBy", required = false, defaultValue = "id") String orderBy,
      @RequestParam(value = "direction", required = false, defaultValue = "ASC") String direction,
      @RequestParam(value = "fields", required = false) String fields,
      @RequestParam(value = "type") String type)
      throws Exception {

    List<SearchCriteria> params = tools.getSearchCriteria(search);
    List<Menu> Menus =
        api.search(params, orderBy, direction, "", new AtomicReference<Boolean>(true), false);
    ByteArrayInputStream bis;
    if (Menus.size() == 0) {
      bis = new ByteArrayInputStream(new byte[0]);
    } else {
      bis = exporter.export(new ArrayList<>(Menus), fields, type);
    }

    HttpHeaders headers = new HttpHeaders();
    switch (type.toLowerCase()) {
      case "pdf":
        headers.add("Content-Disposition", "inline; filename=MenusExport.pdf");
        return ResponseEntity.ok()
            .headers(headers)
            .contentType(MediaType.APPLICATION_PDF)
            .body(new InputStreamResource(bis));
      case "excel":
        headers.add("Content-Disposition", "inline; filename=MenusExport.xls");
        return ResponseEntity.ok()
            .headers(headers)
            .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
            .body(new InputStreamResource(bis));
      default:
        throw new InvalidRequestException("invalid type");
    }
  }

  @GetMapping(value = "/getTemplate")
  @PreAuthorize("hasAuthority('MENU_WRITE_PRIVILEGE')")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<?> getMenuTemplate() throws Exception {
    HttpHeaders headers = new HttpHeaders();
    headers.add("Content-Disposition", "inline; filename=MenuTemplate.csv");
    return ResponseEntity.ok()
        .headers(headers)
        .contentType(MediaType.APPLICATION_OCTET_STREAM)
        .body(new InputStreamResource(converter.getTemplate()));
  }

  @ResponseStatus(HttpStatus.OK)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public Map<String, String> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
    Map<String, String> errors = new HashMap<>();

    ex.getBindingResult()
        .getFieldErrors()
        .forEach(
            error -> {
              errors.put(error.getField(), error.getDefaultMessage());
              System.out.println(
                  "error.getField()::"
                      + error.getField()
                      + " error.getDefaultMessage()::"
                      + error.getDefaultMessage());
            });

    return errors;
  }

  // GENERATED METHODS
  @PutMapping("/{id}")
  @PreAuthorize("hasAuthority('MENU_WRITE_PRIVILEGE')")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<Menu> updateMenu(@RequestBody Menu Menu, @PathVariable String id)
      throws Exception {
    Authentication authentication = authenticationFacade.getAuthentication();
    String userid = authentication.getName();
    AtomicReference<Boolean> isAdmin = new AtomicReference<>(false);
    authentication
        .getAuthorities()
        .forEach(
            s -> {
              //     if(s.getAuthority().toLowerCase().contains("admin")) isAdmin.set(true);
              if (userid.equals("admin")) isAdmin.set(true);
            });

    if (api.find(id, false, userid, isAdmin, true) == null) {
      throw new NotFoundException("id not found");
    }
    Menu.setId(id);
    api.update(Menu);

    return ResponseEntity.ok().body(Menu);
  }

  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  @PreAuthorize("hasAuthority('MENU_WRITE_PRIVILEGE')")
  public ResponseEntity<Menu> createMenu(@Valid @RequestBody Menu Menu) throws Exception {
    if (api.findBy("id", Menu.getId()) != null) {
      throw new ConflictException(
          "unique constraint violated, duplicate field: id -> " + Menu.getId());
    }
    Authentication authentication = authenticationFacade.getAuthentication();
    String userid = authentication.getName();

    Menu.setOwnerId(userid);

    Menu.setLastUpdateBy(userid);

    Menu.setDateUpdated(new java.util.Date(System.currentTimeMillis()));

    Menu.setDateAdded(new java.util.Date(System.currentTimeMillis()));

    Menu.setOrder(0);
    api.create(Menu);

    return ResponseEntity.created(URI.create(Menu.getId())).body(Menu);
  }

  @PostMapping(value = "/import")
  @PreAuthorize("hasAuthority('MENU_WRITE_PRIVILEGE')")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<List<Menu>> importMenus(@RequestPart("file") MultipartFile filePart)
      throws Exception {

    List<Menu> Menus =
        converter.csvToPojo(Collections.singletonList(new String(filePart.getBytes())));
    Authentication authentication = authenticationFacade.getAuthentication();
    String userid = authentication.getName();

    for (Menu Menu : Menus) {

      Menu.setOwnerId(userid);

      Menu.setLastUpdateBy(userid);

      Menu.setDateUpdated(new java.util.Date(System.currentTimeMillis()));

      Menu.setDateAdded(new java.util.Date(System.currentTimeMillis()));

      Menu.setOrder(0);
      api.create(Menu);
    }
    return ResponseEntity.ok(Menus);
  }

  @Value("classpath:json/MENU.json")
  Resource menufile;

  @GetMapping(path = "getEntityFields")
  @ResponseStatus(HttpStatus.OK)
  @PreAuthorize("hasAuthority('MENU_READ_PRIVILEGE')")
  public ResponseEntity<String> getEntityFields() throws Exception {
    InputStream EntityListContent = menufile.getInputStream();
    StringBuilder fileContent = new StringBuilder();
    BufferedReader bufferedReader = null;
    try {
      bufferedReader =
          //        new BufferedReader(new FileReader(new
          // File(String.valueOf(menufile.getFile()))));
          new BufferedReader(new InputStreamReader(EntityListContent));

      String line;
      while ((line = bufferedReader.readLine()) != null) {
        fileContent.append(line);
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (bufferedReader != null) {
        try {
          bufferedReader.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    String arrayToJson = fileContent.toString();
    return ResponseEntity.ok().body(arrayToJson);
  }

  @PatchMapping(path = "/{id}")
  public ResponseEntity<Menu> updateMenu(
      @PathVariable String id, @RequestBody Map<String, Object> patch) {
    try {
      Authentication authentication = authenticationFacade.getAuthentication();
      String userid = authentication.getName();
      AtomicReference<Boolean> isAdmin = new AtomicReference<>(false);
      authentication
          .getAuthorities()
          .forEach(
              s -> {
                //  if (s.getAuthority().toLowerCase().contains("admin")) isAdmin.set(true);
                if (userid.equals("admin")) isAdmin.set(true);
              });

      Menu customer = api.find(id, false, userid, isAdmin, false);
      if (customer != null) {
        patch.forEach(
            (change, value) -> {
              switch (change) {
                case "id":
                  customer.setId((String) value);
                  break;
                case "OwnerId":
                  customer.setOwnerId((String) value);
                  break;
                case "LastUpdateBy":
                  customer.setLastUpdateBy((String) value);
                  break;
                case "DateUpdated":
                  customer.setDateUpdated((Date) value);
                  break;
                case "DateAdded":
                  customer.setDateAdded((Date) value);
                  break;
                case "FriendlyName":
                  customer.setFriendlyName((String) value);
                  break;
                case "Order":
                  customer.setOrder((int) value);
                  break;
                case "Enabled":
                  customer.setEnabled((Boolean) value);
                  break;
                case "Caption":
                  customer.setCaption((String) value);
                  break;
                case "Url":
                  customer.setUrl((String) value);
                  break;
                case "Entity":
                  customer.setEntity((EntityDef) value);
                  break;
                case "MenuType":
                  customer.setMenuType((MenuType) value);
                  break;
                case "ParentId":
                  customer.setParentId((String) value);
                  break;
                case "Icon":
                  customer.setIcon((String) value);
                  break;
                case "EndPoint":
                  customer.setEndPoint((String) value);
                  break;
              }
            });
        api.update(customer);
      }
      return ResponseEntity.ok(customer);
    } catch (Exception e) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
  }
}
