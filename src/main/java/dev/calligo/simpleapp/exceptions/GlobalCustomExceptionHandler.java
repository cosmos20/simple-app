package dev.calligo.simpleapp.exceptions;

import dev.calligo.simpleapp.exceptions.global.*;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalCustomExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(value = Exception.class)
  public ResponseEntity<EntityErrorResponse> defaultErrorHandler(
      HttpServletRequest req, Exception ex) throws Exception {
    if (AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class) != null) throw ex;

    EntityErrorResponse error =
        new EntityErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
    ex.printStackTrace();
    return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(value = NotFoundException.class)
  public final ResponseEntity<EntityErrorResponse> notFoundExceptionHandler(
      Exception ex, WebRequest request) {
    EntityErrorResponse error =
        new EntityErrorResponse(HttpStatus.NOT_FOUND.value(), ex.getMessage());
    return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(value = InternalException.class)
  public final ResponseEntity<EntityErrorResponse> internalExceptionHandler(
      Exception ex, WebRequest request) {
    EntityErrorResponse error =
        new EntityErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
    ex.printStackTrace();
    return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(value = UpstreamCommunicationException.class)
  public final ResponseEntity<EntityErrorResponse> brokerCommunicationExceptionHandler(
      Exception ex, WebRequest request) {
    EntityErrorResponse error =
        new EntityErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage());
    return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(value = ForbiddenException.class)
  public final ResponseEntity<EntityErrorResponse> forbiddenExceptionHandler(
      Exception ex, WebRequest request) {
    EntityErrorResponse error =
        new EntityErrorResponse(HttpStatus.FORBIDDEN.value(), ex.getMessage());
    return new ResponseEntity<>(error, HttpStatus.FORBIDDEN);
  }

  @ExceptionHandler(value = UnauthorizedException.class)
  public final ResponseEntity<EntityErrorResponse> unauthorizedExceptionHandler(
      Exception ex, WebRequest request) {
    EntityErrorResponse error =
        new EntityErrorResponse(HttpStatus.UNAUTHORIZED.value(), ex.getMessage());
    return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
  }

  @ExceptionHandler(value = AccessDeniedException.class)
  public final ResponseEntity<EntityErrorResponse> accessDeniedExceptionHandler(
      Exception ex, WebRequest request) {
    if (request.getHeader("Authorization") == null) {
      EntityErrorResponse error =
          new EntityErrorResponse(HttpStatus.UNAUTHORIZED.value(), ex.getMessage());
      return new ResponseEntity<>(error, HttpStatus.UNAUTHORIZED);
    }
    EntityErrorResponse error =
        new EntityErrorResponse(HttpStatus.FORBIDDEN.value(), ex.getMessage());
    return new ResponseEntity<>(error, HttpStatus.FORBIDDEN);
  }

  @ExceptionHandler(value = ConflictException.class)
  public final ResponseEntity<EntityErrorResponse> conflictExceptionHandler(
      Exception ex, WebRequest request) {
    EntityErrorResponse error =
        new EntityErrorResponse(HttpStatus.CONFLICT.value(), ex.getMessage());
    return new ResponseEntity<>(error, HttpStatus.CONFLICT);
  }

  @ExceptionHandler(value = InvalidRequestException.class)
  public final ResponseEntity<EntityErrorResponse> invalidRequestExceptionHandler(
      Exception ex, WebRequest request) {
    EntityErrorResponse error =
        new EntityErrorResponse(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
    return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
  }

  @Override
  public final ResponseEntity<Object> handleHttpMessageNotReadable(
      HttpMessageNotReadableException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {
    EntityErrorResponse error =
        new EntityErrorResponse(HttpStatus.BAD_REQUEST.value(), "deserialization error");
    return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
  }
}
