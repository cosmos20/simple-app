package dev.calligo.simpleapp.converters.generated;

import org.springframework.core.convert.converter.Converter;
import dev.calligo.simpleapp.enums.generated.MenuType;

public class StringToMenuTypeConverter implements Converter<String, MenuType> {
  @Override
  public MenuType convert(String source) {
    try {
      return MenuType.valueOf(source.toUpperCase());
    } catch (IllegalArgumentException e) {
      return null;
    }
  }
}
