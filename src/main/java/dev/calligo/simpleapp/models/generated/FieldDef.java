package dev.calligo.simpleapp.models.generated;

import com.github.javafaker.Faker;
import io.swagger.annotations.ApiModel;
import dev.calligo.simpleapp.enums.generated.*;
import lombok.Setter;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.*;
import org.springframework.http.MediaType;
import javax.validation.constraints.*;
import org.springframework.data.mongodb.core.mapping.Field;
import com.google.gson.annotations.SerializedName;

@ApiModel(description = "FieldDef model")
public class FieldDef implements Serializable {
  Map<String, ArrayList<String>> arrayofarray;

  @Getter
  @Setter
  @Field("_id")
  @Id
  @SerializedName("id")
  private String id;

  @Getter
  @Setter
  @SerializedName("OwnerId")
  private String OwnerId;

  @Getter
  @Setter
  @SerializedName("LastUpdateBy")
  private String LastUpdateBy;

  @Getter
  @Setter
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @SerializedName("DateUpdated")
  private Date DateUpdated;

  @Getter
  @Setter
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  @SerializedName("DateAdded")
  private Date DateAdded;

  @NotNull(message = "This field is required")
  @Getter
  @Setter
  @SerializedName("FriendlyName")
  private String FriendlyName;

  @NotNull(message = "This field is required")
  @Getter
  @Setter
  @SerializedName("FieldName")
  private String FieldName;

  @NotNull(message = "This field is required")
  @Getter
  @Setter
  @SerializedName("Type")
  private String Type;

  @NotNull(message = "This field is required")
  @Getter
  @Setter
  @SerializedName("Label")
  private String Label;

  @NotNull(message = "This field is required")
  @Getter
  @Setter
  @SerializedName("Hint")
  private String Hint;

  public FieldDef generateFake() {
    Faker faker = new Faker();
    FieldDef ret = new FieldDef();
    ret.setId(String.valueOf(UUID.randomUUID()));

    ret.setOwnerId("admin");

    ret.setLastUpdateBy(faker.name().name());

    ret.setDateUpdated(new Date());

    ret.setDateAdded(new Date());

    ret.setFriendlyName(faker.name().name());

    ret.setFieldName(faker.name().name());

    ret.setType(faker.name().name());

    ret.setLabel(faker.name().name());

    ret.setHint(faker.name().name());
    return ret;
  }

  public String toCSV(String attach) {
    String ret = attach;
    ret =
        ret
            + (ret.length() > 3 ? "," : "")
            + this.id
            + ","
            + this.OwnerId
            + ","
            + this.LastUpdateBy
            + ","
            + this.DateUpdated
            + ","
            + this.DateAdded
            + ","
            + this.FriendlyName
            + ","
            + this.FieldName
            + ","
            + this.Type
            + ","
            + this.Label
            + ","
            + this.Hint;
    return ret;
  }

  private String leverage(String line, Map<String, ArrayList<String>> arrayofarray) {
    String ret = "";
    Map<String, ArrayList<String>> arrayofarray2 = arrayofarray;
    ArrayList<String> arrayofarray3 = new ArrayList<>();
    arrayofarray3.addAll(arrayofarray.keySet());
    for (String s : arrayofarray3) {
      if (arrayofarray != null && arrayofarray.size() > 0) {
        for (String d : arrayofarray.get(s.toUpperCase())) {
          String ll = line.replaceAll(s.toUpperCase(), d);
          Map<String, ArrayList<String>> arrayofarray22 = arrayofarray2;
          arrayofarray22.remove(s);
          if (arrayofarray22.size() > 0) ret = ret + "" + leverage(ll, arrayofarray22);
          else ret = ret + "" + ll;
        }
      }
    }

    return ret;
  }

  private String toleverage(ArrayList<String> clientproducts, String replacement) {
    if (arrayofarray == null) arrayofarray = new HashMap<>();
    if (clientproducts != null && clientproducts.size() > 0) {
      ArrayList<String> datas = arrayofarray.get(replacement);
      if (datas == null) datas = new ArrayList<>();
      for (String s : clientproducts) datas.add(s);
      arrayofarray.put(replacement, datas);
    } else replacement = "";
    return replacement;
  }

  @Override
  public String toString() {
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      return objectMapper.writeValueAsString(this);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
      return null;
    }
  }
}
