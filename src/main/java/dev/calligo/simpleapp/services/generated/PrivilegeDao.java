package dev.calligo.simpleapp.services.generated;

import dev.calligo.simpleapp.models.generated.*;
import dev.calligo.simpleapp.models.generated.Privilege;
import dev.calligo.simpleapp.enums.generated.*;
import dev.calligo.simpleapp.search.SearchCriteria;
import org.springframework.data.domain.Page;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public interface PrivilegeDao {

  /*
   create single Privilege
  */
  Privilege create(Privilege Privilege);

  /*
   create multiple Privileges
  */
  List<Privilege> create(List<Privilege> Privileges);

  /*
   update single Privilege
  */
  Privilege update(Privilege Privilege);

  /*
   delete single Privilege
  */
  void delete(String id);

  /*
   delete multiple Privileges
  */
  void deleteMany(List<String> ids);

  /*
   delete all Privileges
  */
  void deleteAll();

  /*
   find user by id
  */
  Privilege find(
      String Id, boolean cache, String userid, AtomicReference<Boolean> isAdmin, Boolean hasfield);

  /*
   search for Privileges using search criteria
  */
  Page<Privilege> search(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      int page,
      int size,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  long count(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      int size,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  /*
   search for Privileges using search criteria - non paginated
  */
  List<Privilege> search(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  /*
   find by field
  */
  Privilege findBy(String fieldName, String value);

  List<Privilege> getall(ArrayList<String> fieldNames);
}
