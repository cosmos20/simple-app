package dev.calligo.simpleapp.converters.generated;

import dev.calligo.simpleapp.models.generated.EntityDef;
import dev.calligo.simpleapp.models.generated.FieldDef;
import dev.calligo.simpleapp.utils.EntitiesUtil;
import dev.calligo.simpleapp.models.generated.EntityDef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.Getter;
import lombok.Setter;
import com.google.gson.Gson;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class EntityDefConverter {

  @Setter @Getter @Autowired EntitiesUtil entitiesUtil;

  @Autowired
  public EntityDefConverter() {}

  public List<EntityDef> csvToPojo(List<String> list) {
    List<EntityDef> finalList = new ArrayList<>();
    for (String line : list.subList(1, -1)) {
      try {
        EntityDef m = convertToEntityDef(line);
        finalList.add(m);
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
    return finalList;
  }

  public ByteArrayInputStream getTemplate() {
    EntityDef en = entitiesUtil.entityMap.get("EntityDef");
    Gson gson = new Gson();
    String jsonInString = gson.toJson(en);
    return new ByteArrayInputStream(jsonInString.getBytes(StandardCharsets.UTF_8));
    //    en.getFields().sort(Comparator.comparing(FieldDef::getName));
    //    String[] headers = new String[en.getFields().size()];
    //    int i = 0;
    //    for (FieldDef f : en.getFields()) {
    //      headers[i] = escapeSpecialCharacters(f.getName() + "(" + f.getType() + ")");
    //      i++;
    //    }
    //    return new ByteArrayInputStream(String.join("\t",
    // headers).getBytes(StandardCharsets.UTF_8));
  }

  public String escapeSpecialCharacters(String data) {
    String escapedData = data.replaceAll("\\R", " ");
    if (data.contains(",") || data.contains("\"") || data.contains("'")) {
      data = data.replace("\"", "\"\"");
      escapedData = "\"" + data + "\"";
    }
    return escapedData;
  }

  public EntityDef convertToEntityDef(String line) {
    //    EntityDef en = entitiesUtil.entityMap.get("EntityDef");
    //    en.getFields().sort(Comparator.comparing(FieldDef::getName));
    //    int i = 0;
    //    for (FieldDef f : en.getFields()) {
    //
    //    }
    return new EntityDef();
  }
}
