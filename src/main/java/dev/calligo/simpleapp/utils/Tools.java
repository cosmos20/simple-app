package dev.calligo.simpleapp.utils;

import dev.calligo.simpleapp.search.SearchCriteria;
import com.google.common.base.CaseFormat;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class Tools {

  public Tools() {}

  public String normalizeMSISDN(String in) {
    if (!in.startsWith("+")) {
      if (in.startsWith("00")) {
        in = "+" + in.substring(2);
      } else {
        in = "+" + in;
      }
    }
    return in;
  }

  public List<SearchCriteria> getSearchCriteria(String search) {
    List<SearchCriteria> params = new ArrayList<>();
    if (search != null) {
      Pattern pattern = Pattern.compile("((\\w+)([\\.]\\w+)?)(:|>|<|~)(.*)");
      for (String searchCriteria : search.split(",")) {
        Matcher matcher = pattern.matcher(searchCriteria);
        while (matcher.find()) {
          params.add(new SearchCriteria(matcher.group(1), matcher.group(4), matcher.group(5)));
        }
      }
    }
    return params;
  }

  public String toDashLimitedPlural(String myString) {
    try {
     // return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_HYPHEN, myString) + "s";
      return myString + "s";
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public String toDashLimitedSingular(String myString) {
    try {
      return myString;
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}
