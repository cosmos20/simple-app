package dev.calligo.simpleapp.utils;

import dev.calligo.simpleapp.models.Entity;
import dev.calligo.simpleapp.models.generated.*;
import lombok.Data;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Data
@Setter
@Component
public class EntitiesUtil {
    public ConcurrentHashMap<String, EntityDef> entityMap;
    public ConcurrentHashMap<String, Entity> entityMapEntity;

    @Autowired
    public EntitiesUtil() {

        entityMap = new ConcurrentHashMap<>();
        entityMapEntity = new ConcurrentHashMap<>();
    }

    public boolean isPrimitiveDataType(String dataType) {
        if (dataType == null) return true;
        if (dataType.contains(">") || dataType.contains("<")) {
            dataType = dataType.substring(dataType.indexOf('<'), dataType.indexOf('>'));
        }
        if (dataType.toLowerCase().equals("string")) return true;
        if (dataType.toLowerCase().equals("float")) return true;
        if (dataType.toLowerCase().equals("boolean")) return true;
        if (dataType.toLowerCase().equals("byte")) return true;
        if (dataType.toLowerCase().equals("char")) return true;
        if (dataType.toLowerCase().equals("short")) return true;
        if (dataType.toLowerCase().equals("date")) return true;
        if (dataType.toLowerCase().equals("int")) return true;
        if (dataType.toLowerCase().equals("long")) return true;
        if (dataType.toLowerCase().equals("double")) return true;
        if (dataType.toLowerCase().equals("null")) return true;
        return dataType.toLowerCase().length() < 2;
    }

    public String getEntityType(String dataType) {
        if (dataType.contains(">") || dataType.contains("<")) {
            return dataType.substring(dataType.indexOf('<'), dataType.indexOf('>'));
        }
        return dataType;
    }

    public Boolean isArrayType(String dataType) {
        if (dataType.contains(">") || dataType.contains("<")) {
            return true;
        }
        return false;
    }
}
