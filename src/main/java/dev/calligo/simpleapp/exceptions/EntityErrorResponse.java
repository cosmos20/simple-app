package dev.calligo.simpleapp.exceptions;

import lombok.Data;

import java.util.Date;

@Data
public class EntityErrorResponse {
  private Date timestamp;
  private Integer status;
  private String message;

  public EntityErrorResponse(Integer st, String msg) {
    timestamp = new Date(System.currentTimeMillis());
    status = st;
    message = msg;
  }
}
