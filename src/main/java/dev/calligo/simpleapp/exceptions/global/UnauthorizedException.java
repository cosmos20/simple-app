package dev.calligo.simpleapp.exceptions.global;

public class UnauthorizedException extends RuntimeException {
  public UnauthorizedException(String msg) {
    super(msg);
  }
}
