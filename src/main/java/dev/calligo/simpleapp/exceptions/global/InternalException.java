package dev.calligo.simpleapp.exceptions.global;

public class InternalException extends RuntimeException {
  public InternalException(String msg) {
    super(msg);
  }
}
