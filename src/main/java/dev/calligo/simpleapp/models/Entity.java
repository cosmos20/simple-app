package dev.calligo.simpleapp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Setter
public class Entity {

  public String entityName;
  public boolean hasDB = false;
  public boolean hasExport = false;
  public boolean hasDeleteAll = false;
  public boolean hasView = false;
  public boolean hasId = false;
  public boolean enumerated ;
  public String inClass = "";
  public String afterCreate = "";
  public String beforeCreate = "";
  public String afterUpdate = "";
  public String beforeUpdate = "";
  public String autowire = "";

  public String type = "Class";
  public List<EntityField> fields;
  public Map<String, EntityField> fieldMap = new HashMap<>();
  @JsonIgnore public List<EntityField> ownerFields;
  @JsonIgnore public List<EntityField> externalDependencyFields;
  @JsonIgnore public List<EntityField> uniqueFields;
  @JsonIgnore public List<EntityField> defaultValueFields;
  public boolean hasVersioning = false;
  public String imports = "";

  public Entity() {
    fields = new ArrayList<>();
    ownerFields = new ArrayList<>();
    externalDependencyFields = new ArrayList<>();
    uniqueFields = new ArrayList<>();
    defaultValueFields = new ArrayList<>();
  }

  @Override
  public String toString() {
    return '{'
        + "entityName='"
        + entityName
        + '\''
        + ", hasDB="
        + hasDB
        + ", hasExport="
        + hasExport
        + ", hasDeleteAll="
        + hasDeleteAll
        + ", hasView="
        + hasView
        + ", hasId="
        + hasId
        + ", inClass='"
        + inClass
        + '\''
        + ", autowire='"
        + autowire
        + '\''
        + ", afterCreate='"
        + afterCreate
        + '\''
        + ", beforeCreate='"
        + beforeCreate
        + '\''
        + ", afterUpdate='"
        + afterUpdate
        + '\''
        + ", beforeUpdate='"
        + beforeUpdate
        + '\''
        + ", type='"
        + type
        + '\''
        + ", hasVersioning="
        + hasVersioning
        + '}';
  }
}
