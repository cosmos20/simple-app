package dev.calligo.simpleapp.services.generated;

import dev.calligo.simpleapp.models.generated.*;
import dev.calligo.simpleapp.models.generated.Menu;
import dev.calligo.simpleapp.enums.generated.*;
import dev.calligo.simpleapp.search.SearchCriteria;
import org.springframework.data.domain.Page;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public interface MenuDao {

  /*
   create single Menu
  */
  Menu create(Menu Menu);

  /*
   create multiple Menus
  */
  List<Menu> create(List<Menu> Menus);

  /*
   update single Menu
  */
  Menu update(Menu Menu);

  /*
   delete single Menu
  */
  void delete(String id);

  /*
   delete multiple Menus
  */
  void deleteMany(List<String> ids);

  /*
   delete all Menus
  */
  void deleteAll();

  /*
   find user by id
  */
  Menu find(
      String Id, boolean cache, String userid, AtomicReference<Boolean> isAdmin, Boolean hasfield);

  /*
   search for Menus using search criteria
  */
  Page<Menu> search(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      int page,
      int size,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  long count(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      int size,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  /*
   search for Menus using search criteria - non paginated
  */
  List<Menu> search(
      List<SearchCriteria> params,
      String orderBy,
      String direction,
      String userid,
      AtomicReference<Boolean> isAdmin,
      Boolean hasfield)
      throws Exception;

  /*
   find by field
  */
  Menu findBy(String fieldName, String value);

  List<Menu> getall(ArrayList<String> fieldNames);
}
