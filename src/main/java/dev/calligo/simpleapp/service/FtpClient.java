package dev.calligo.simpleapp.service;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

public class FtpClient {

  private String server;
  private int port;
  private String user;
  private String password;
  private FTPClient ftp;

  public FtpClient(String server, int port, String user, String password) {
    this.server = server;
    this.port = port;
    this.user = user;
    this.password = password;
  }

  // constructor

  public void download(String remoteFile1, String downloadpath) throws IOException {
    FTPClient ftpClient = new FTPClient();
    try {

      ftpClient.connect("server", port);
      ftpClient.login(user, password);
      ftpClient.enterLocalPassiveMode();
      ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

      // APPROACH #1: using retrieveFile(String, OutputStream)
      File downloadFile1 = new File(downloadpath);
      OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadFile1));
      boolean success = ftpClient.retrieveFile(remoteFile1, outputStream1);
      outputStream1.close();

      if (success) {
        System.out.println("File #1 has been downloaded successfully.");
      }

      //            // APPROACH #2: using InputStream retrieveFileStream(String)
      //            String remoteFile2 = "/test/song.mp3";
      //            File downloadFile2 = new File("D:/Downloads/song.mp3");
      //            OutputStream outputStream2 = new BufferedOutputStream(new
      // FileOutputStream(downloadFile2));
      //            InputStream inputStream = ftpClient.retrieveFileStream(remoteFile2);
      //            byte[] bytesArray = new byte[4096];
      //            int bytesRead = -1;
      //            while ((bytesRead = inputStream.read(bytesArray)) != -1) {
      //                outputStream2.write(bytesArray, 0, bytesRead);
      //            }
      //
      //            success = ftpClient.completePendingCommand();
      //            if (success) {
      //                System.out.println("File #2 has been downloaded successfully.");
      //            }
      //            outputStream2.close();
      //            inputStream.close();

    } catch (IOException ex) {
      System.out.println("Error: " + ex.getMessage());
      ex.printStackTrace();
    } finally {
      try {
        if (ftpClient.isConnected()) {
          ftpClient.logout();
          ftpClient.disconnect();
        }
      } catch (IOException ex) {
        ex.printStackTrace();
      }
    }
  }

  public void upload(MultipartFile firstLocal) throws IOException {
    FTPClient ftpClient = new FTPClient();
    try {

      ftpClient.connect(server, port);
      ftpClient.login(user, password);
      ftpClient.enterLocalPassiveMode();
      ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

      // APPROACH #1: uploads first file using an InputStream
      //            File firstLocalFile = firstLocal.getResource().getFile();
      //
      //            String firstRemoteFile = firstLocal.getName();
      InputStream inputStream = firstLocal.getInputStream();

      System.out.println("Start uploading first file");
      boolean done = ftpClient.storeFile(firstLocal.getOriginalFilename(), inputStream);
      inputStream.close();
      if (done) {
        System.out.println(
            "The first file::" + firstLocal.getOriginalFilename() + ":: is uploaded successfully.");
      }

      //            // APPROACH #2: uploads second file using an OutputStream
      //            File secondLocalFile = new File("E:/Test/Report.doc");
      //            String secondRemoteFile = "test/Report.doc";
      //            inputStream = new FileInputStream(secondLocalFile);
      //
      //            System.out.println("Start uploading second file");
      //            OutputStream outputStream = ftpClient.storeFileStream(secondRemoteFile);
      //            byte[] bytesIn = new byte[4096];
      //            int read = 0;
      //
      //            while ((read = inputStream.read(bytesIn)) != -1) {
      //                outputStream.write(bytesIn, 0, read);
      //            }
      //            inputStream.close();
      //            outputStream.close();

      //            boolean completed = ftpClient.completePendingCommand();
      //            if (completed) {
      //                System.out.println("The second file is uploaded successfully.");
      //            }

    } catch (IOException ex) {
      System.out.println("Error: " + ex.getMessage());
      ex.printStackTrace();
    } finally {
      try {
        if (ftpClient.isConnected()) {
          ftpClient.logout();
          ftpClient.disconnect();
        }
      } catch (IOException ex) {
        ex.printStackTrace();
      }
    }
  }

  public void upload(InputStream inputStream, String name) throws IOException {
    FTPClient ftpClient = new FTPClient();
    try {

      ftpClient.connect(server, port);
      ftpClient.login(user, password);
      ftpClient.enterLocalPassiveMode();
      ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

      // APPROACH #1: uploads first file using an InputStream
      //            File firstLocalFile = firstLocal.getResource().getFile();
      //
      //            String firstRemoteFile = firstLocal.getName();
      //    InputStream inputStream =firstLocal.getInputStream();

      System.out.println("Start uploading first file");
      boolean done = ftpClient.storeFile(name, inputStream);
      inputStream.close();
      if (done) {
        System.out.println("The first file::" + name + ":: is uploaded successfully.");
      }

      //            // APPROACH #2: uploads second file using an OutputStream
      //            File secondLocalFile = new File("E:/Test/Report.doc");
      //            String secondRemoteFile = "test/Report.doc";
      //            inputStream = new FileInputStream(secondLocalFile);
      //
      //            System.out.println("Start uploading second file");
      //            OutputStream outputStream = ftpClient.storeFileStream(secondRemoteFile);
      //            byte[] bytesIn = new byte[4096];
      //            int read = 0;
      //
      //            while ((read = inputStream.read(bytesIn)) != -1) {
      //                outputStream.write(bytesIn, 0, read);
      //            }
      //            inputStream.close();
      //            outputStream.close();

      //            boolean completed = ftpClient.completePendingCommand();
      //            if (completed) {
      //                System.out.println("The second file is uploaded successfully.");
      //            }

    } catch (IOException ex) {
      System.out.println("Error: " + ex.getMessage());
      ex.printStackTrace();
    } finally {
      try {
        if (ftpClient.isConnected()) {
          ftpClient.logout();
          ftpClient.disconnect();
        }
      } catch (IOException ex) {
        ex.printStackTrace();
      }
    }
  }
}
